# extract images in multiple levels of subfolders and stitch them into one larger image whose name is the concatenated path of all folders directing to the multiple sub-images
# all sub-images must be within the same leaf level subfolder

# William WANG, Griffith IIIS

import cv2
import numpy as np
from glob import glob
from multiprocessing import Pool
from os import path

imgWidth = 1920
imgHeight = 1200
imgDepth = 3
dummyImg = np.zeros((imgHeight, imgWidth, imgDepth), np.uint8)
liBlockImageNames = ['Image1.jpg', 'Image2.jpg', 'Image3.jpg', 'Image4.jpg']

def walk_punnets(path):
    listof_punnets = []
    for f in glob(path+'/**/', recursive=True):
        #print(f)
        listof_punnets.append(f)
    #print(len(listof_images))
    return listof_punnets

def create_punnet_img(punnetPath):
    listPunnetPath = punnetPath.split('/')
    punnetName = '_'.join(listPunnetPath[1:])+'.jpg'
    
    def _read_img(punPath, imgName):
        blockName = punPath + imgName
        #print(blockName)
        if path.exists(blockName):
            im = cv2.imread(blockName)
            if im is not None:
                blockImg = cv2.resize(im, (imgWidth, imgHeight))
                return blockImg
        return dummyImg    

    #Read image
    blockImages = []
    for iName in liBlockImageNames:
        blockImage = _read_img(punnetPath, iName)
        blockImages.append(blockImage)
    #Stick blocks
    row1 = np.hstack((blockImages[0], blockImages[1]))
    row2 = np.hstack((blockImages[2], blockImages[3]))
    fullImg = np.vstack((row1, row2))

    #Write stitched image
    cv2.imwrite(punnetName, fullImg)

    #print(punnetName)

if __name__ == '__main__':
    liPuns = walk_punnets('.')
    p = Pool(16)
    p.map(create_punnet_img, liPuns)
