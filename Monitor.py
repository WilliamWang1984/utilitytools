import os

class Monitor:
    def __init__(self):
        self.debug = False

    def get_monitor_resolutions(self):
        listof_monitors_inuse = os.popen("xrandr | grep '*'").read()
        #print(listof_monitors_inuse)
        monitors = listof_monitors_inuse.split('\n')
        #print(monitors)
        resol_x = []
        resol_y = []
        for monitor in monitors:
            monitor_params = monitor.split()
            for param in monitor_params:
                if 'x' in param:
                    #print('Monitor resolution is:')
                    #print(param)
                    xres, yres = param.split('x')
                    resol_x.append(int(xres))
                    resol_y.append(int(yres))
        return resol_x, resol_y


    def get_nearest_element_in_list(self, x, xList):
        return min(xList, key=lambda n: abs(n - x))


    def get_target_resolutions(self, saved_resol_x, saved_resol_y):
        listof_resol_xs, listof_resol_ys = self.get_monitor_resolutions()
        #print("Printing list res ys, y, list res xs, x")
        #print(listof_resol_ys)
        #print(saved_resol_y)
        #print(listof_resol_xs)
        #print(saved_resol_x)

        nearest_resol_x = self.get_nearest_element_in_list(saved_resol_x, listof_resol_xs)
        #nearest_resol_y = self.get_nearest_element_in_list(saved_resol_y, listof_resol_ys)
        removingIndex = listof_resol_xs.index(nearest_resol_x)

        #print(nearest_resol_y)
        #print(nearest_resol_x)
        if len(listof_resol_xs) > 1 and len(listof_resol_ys) > 1:
            #listof_resol_xs.remove(nearest_resol_x)
            del listof_resol_xs[removingIndex]
            del listof_resol_ys[removingIndex]

        return listof_resol_xs, listof_resol_ys
