#!/bin/bash
# Courtesy of https://superuser.com/questions/644272/how-do-i-delete-all-files-smaller-than-a-certain-size-in-all-subfolders

# This script moves files less than threshold value (bytesize) to destination folder
# Adjust file extension "e.g. *.jpg", file size "e.g. -250k (less than 250k)", destination folder "e.g. ../SmallFiles"
# Copy the script to side by side with the source folder(s) containing many images, some of which are smaller than 250k (not containing sufficient pixel information)
# Run the script 'sh filter_files.sh'
# Get the results in destination folder, source folder is now cleaned of files smaller than 250k

mv $(find . -name "*.jpg" -type 'f' -size -250k) ../SmallFiles 
