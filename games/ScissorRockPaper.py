import random

choices = ['scissor', 'rock', 'paper']

relWheel = {'scissor': ['rock', 'paper'],
            'rock': ['paper', 'scissor'],
            'paper': ['scissor', 'rock']}

computer_score = 0
player_score = 0

computer = random.choice(choices)
player = None

while True:
    player = input("scissor, rock or paper\n")
    if player=='q':
        break
    elif relWheel[player] == relWheel[computer]:
        print(player, computer)
        print("A draw")
    elif relWheel[player][0]==computer:
        print(player, computer)
        print("computer won")
        computer_score += 1
    elif relWheel[player][1]==computer:
        print(player, computer)
        print("player won")
        player_score += 1
    else:
        print("invalid choice")
    print('\n')

    computer = random.choice(choices)

print(player_score, computer_score)

