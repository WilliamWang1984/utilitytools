import pyttsx3, PyPDF2

pdfReader = PyPDF2.PdfFileReader(open('to_read.pdf','rb'))
speaker = pyttsx3.init()

for pnum in range(pdfReader.numPages):
    text = pdfReader.getPage(pnum).extractText()
    speaker.say(text)
    speaker.runAndWait()
speaker.stop()

