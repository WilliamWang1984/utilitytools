import random

passlen = int(input("How long is the password?"))
s = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()?"
s1 = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

specialchar = input("Contain special character?")

#print(specialchar)

if specialchar in ['True', 'true', 'T', 't', 'Yes', 'yes', 'Y', 'y'] :
    p = "".join(random.sample(s, passlen))
else:
    p = "".join(random.sample(s1, passlen))

print(p)
