import time
#import datetime
from datetime import date, datetime, timedelta

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"
ARB_END_DATETIME = datetime.strptime('2050-07-01 12:00:00.000000', DATETIME_FORMAT) # Arbitrary end datetime in DB searching

#print(dir(datetime.date))

TODAY = date.today()
YESTERDAY = TODAY - timedelta(days=1)
TOMORROW = TODAY + timedelta(days=1)

class Timer:
    def __init__(self, key_format='UNIX_EPOCH'):
        self.startTime = TODAY
        self.endTime = ARB_END_DATETIME
        self.key_format = key_format
        
        
    def get_datetime_now(self):
        if self.key_format == 'UNIX_EPOCH':
            return str(int(time.time()))
        elif self.key_format == 'YYYYMMDD':
            return str(datetime.now())
        else:
            raise Exception('Unsupported Timing format (must be UNIX_EPOCH / YYYYMMDD)', self.key_format)
            
            
    def get_datetime_hours_ago(self, n=1):
        if self.key_format == 'UNIX_EPOCH':
            return str(int(time.time())-3600)
        elif self.key_format == 'YYYYMMDD':
            return str(datetime.now() - timedelta(hours = n))
        else:
            raise Exception('Unsupported Timing format (must be UNIX_EPOCH / YYYYMMDD)', self.key_format)
   
   
    def unixepoch_to_yyyymmdd(self, epoch_time):
        return str(datetime.fromtimestamp(int(epoch_time)))
                    

    def datetime_to_timestamp(self, dtAsDatetime):
        return (time.mktime(dtAsDatetime.timetuple())*1e3 + dtAsDatetime.microsecond/1e3)/1e3


    def timestamp_to_datetime(self, tsAsTimestamp):
        return datetime.fromtimestamp(tsAsTimestamp)


    def epoch_to_yyyymmdd(self, epoch_str, have_prefix='False', strip_fractional='True', output_have_colon='True'):
       
        epoch_str1 = epoch_str
        #print(epoch_str, have_prefix, have_fractional)
        if have_prefix == 'True':
            ls_epochs = epoch_str1.split('_')
            epoch_str1 = ls_epochs[-1]

        epoch_str2 = epoch_str1
        if strip_fractional == 'True':
            #print("before dot split: ", epoch_str2)
            ls_epochs = epoch_str2.split('.')
            epoch_str2 = ls_epochs[0] # non-fractional element
            #print("after dot split: ", epoch_str2)

        epoch_str3 = str(datetime.fromtimestamp(float(epoch_str2)))
        if not output_have_colon == 'True':
            epoch_str3 = epoch_str3.replace(':', '_')

        return epoch_str3


    '''
    Given from_Datetime, to_Datetime, datetime delta details e.g. {'second', 10}, generate lists of 
    [startTime, startTime + stepLength, startTime + 2*stepLength, ...]
    [startTime, startTime + stepLenght, startTime + stepLength, startTime + 2*stepLength, ...]
    '''
    def datetime_slice(self, startDatetime, stepLength, nSteps=20, endDatetime=ARB_END_DATETIME):
        xData_nonrepeat = []
        xData_repeat = []

        startDt = startDatetime
        endDt = endDatetime
        stepLnKey = next(iter(stepLength.keys()))
        stepLnVal = next(iter(stepLength.values()))

        #print("step length keys:")
        #print(stepLnKey)
        #print("step length values:")
        #print(stepLnVal)

        stepLn = {}
        stepLn[stepLnKey] = abs(stepLnVal)

        # If user input has wrong direction then swap
        if startDt > endDt: startDt, endDt = endDt, startDt

        # Argument stepLength is expected to be a dictionary item in the form of {<time_unit as string>: <#units as int>}
        nextDatetime = startDt

        xData_nonrepeat.append(startDt) # append a
        xData_repeat.append(startDt) # append a

        # Priority given to startDt, endDt input schemes
        if endDt != ARB_END_DATETIME:

            sanityCounter = 0
            while nextDatetime < endDt and sanityCounter < 1000:
                sanityCounter += 1
                nextDatetime += datetime.timedelta(**stepLn) # If stepLength == {'a':1}, func(**stepLength) will translate to func(a=1)
                if nextDatetime < endDt:
                    xData_nonrepeat.append(nextDatetime) # append b, c, d
                    xData_repeat.append(nextDatetime) # append b, c, d twice
                    xData_repeat.append(nextDatetime)

            if sanityCounter >= 1000: raise Exception("Broke from an infinite while loop, check while condition")

            xData_nonrepeat.append(endDt) # append e
            xData_repeat.append(endDt)

        elif endDt == ARB_END_DATETIME:
            # If no valid endDatetime specified, refer customised nSteps and If no customised nSteps found, use default nSteps
            for i in range(nSteps-1): # range in [0, nSteps-2]
                nextDatetime += datetime.timedelta(**stepLength)
                xData_nonrepeat.append(nextDatetime) # append b, c, d, e
                if i < nSteps-2:
                    xData_repeat.append(nextDatetime) # append b, b, c, c, d, d, if not the last index
                    xData_repeat.append(nextDatetime)
                elif i == nSteps-2:
                    xData_repeat.append(nextDatetime) # append e only once if last index


        # stepLength of type dict with example value: "{'minutes':1}" "{'hours':1}" "{'days':1}", dictionary values can be negative to go unclock-wise in time.
        return xData_nonrepeat, xData_repeat
    

    '''
    Given from_Datetime, to_Datetime of type Python Datetime.Datetime, number of time periods to split, 
    return the list of from_Datetime1, to_Datetime1, from_Datetme2, to_Datetime2, from_Datetime3, to_Datetime3, ... 
    '''
    def datetime_split(self, startDatetime, endDatetime, nSplits):

        xData = []
        queryLists = []

        start = datetime.strptime(str(startDatetime), DATETIME_FORMAT)
        end = datetime.strptime(str(endDatetime), DATETIME_FORMAT)

        tsStart = self.datetime_to_timestamp(start)
        tsEnd = self.datetime_to_timestamp(end)
        delta = (tsEnd - tsStart) / nSplits

        for i in range(nSplits):
            x = self.timestamp_to_datetime(tsStart + delta * i).strftime(DATETIME_FORMAT)
            xData.append(x)   # append a, b, c, d
        xData.append(self.timestamp_to_datetime(tsEnd).strftime(DATETIME_FORMAT)) # append e

        queryLists.append(self.timestamp_to_datetime(tsStart).strftime(DATETIME_FORMAT)) # append a
        for i in range(nSplits-1):
            y = self.timestamp_to_datetime(tsStart + delta * (i+1)).strftime(DATETIME_FORMAT)
            queryLists.append(y) # append b, b, c, c, d, d
            queryLists.append(y)
        queryLists.append(self.timestamp_to_datetime(tsEnd).strftime(DATETIME_FORMAT)) # append e

        return xData, queryLists


    '''
    Given start time (datetimeA), end time (datetimeB), compute delta (B-A) and shift current time (datetimeX) into future time (datetimeX + delta)
    '''    
    def datetime_shift(self, datetimeA, datetimeB, datetimeX):
        tsA = self.datetime_to_timestamp(datetimeA)
        tsB = self.datetime_to_timestamp(datetimeB)
        tsX = self.datetime_to_timestamp(datetimeX)
        delta = tsB - tsA
        datetimeY = self.timestamp_to_datetime(tsX + delta).strftime(DATETIME_FORMAT)

        return datetimeY

    '''
    Name a file from current system's datetime time as string
    '''
    def get_filename_from_datetime(self, dtString):
        strNow = datetime.now()
        
        strYear = str(dtString.year) if hasattr(dtString, 'year') else str(strNow.year)
        strMonth = str(dtString.month).zfill(2) if hasattr(dtString, 'month') else str(strNow.month).zfill(2)
        strDay = str(dtString.day).zfill(2) if hasattr(dtString, 'day') else str(strNow.day).zfill(2)
        strHour = str(dtString.hour).zfill(2) if hasattr(dtString, 'hour') else str(strNow.hour).zfill(2)
        strMinute = str(dtString.minute).zfill(2) if hasattr(dtString, 'minute') else str(strNow.minute).zfill(2)
        strSecond = str(dtString.second).zfill(2) if hasattr(dtString, 'second') else str(strNow.second).zfill(2)
        return ''.join([strYear, strMonth, strDay, '_', strHour, strMinute, strSecond])


