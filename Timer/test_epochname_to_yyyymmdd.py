import time
from datetime import datetime
from glob import glob
import os

from Timer import Timer
import sys

if not '/home/william/utilitytools' in sys.path:
    sys.path.append('/home/william/utilitytools')

from Argparser.Argparser import Argparser

def epoch_to_yyyymmdd(epoch_str, have_rcwy_id='False', have_fractional='True'):
   
    epoch_str1 = epoch_str
    print(epoch_str, have_rcwy_id, have_fractional)
    if have_rcwy_id == 'True':
        ls_epochs = epoch_str1.split('_')
        epoch_str1 = ls_epochs[-1]

    epoch_str2 = epoch_str1
    if have_fractional == 'True':
        print("before dot split: ", epoch_str2)
        ls_epochs = epoch_str2.split('.')
        epoch_str2 = ls_epochs[0]
        print("after dot split: ", epoch_str2)

    #ls_epochs = epoch_str.split('_') if have_rcwy_id else epoch_str
    return str(datetime.fromtimestamp(int(epoch_str2)))


def extract_epoch(path_name):
    ls_fnames = path_name.split('/')
    #print(ls_fnames[-1])
    fname = ls_fnames[-1]
    pre, ext = fname.split('.jp')
    return pre, ext

def replace_filename(src_str, src, dst):
    dst_str = src_str.replace(src, dst)
    print(dst_str)
    return dst_str

if __name__ == '__main__':

    #files_path = glob('./images/*.*')

    ap1 = Argparser()
    ls1 = ap1.compre_sys_argv(sys.argv[1:])
    #print(ls1)

    tim1 = Timer()


    glob_path = './images/*.jpg'
    have_rcwy_id='True'
    have_fractional='True'
    
    for tup_i in ls1:
        #print(tup_i)

        glob_path = tup_i[1] if tup_i[0] == 'file_path' else glob_path
        have_rcwy_id = tup_i[1] if tup_i[0] == 'have_source_id' else have_rcwy_id
        have_fractional = tup_i[1] if tup_i[0] == 'have_fractional' else have_fractional
    
    files_path = glob(glob_path)

    print(len(files_path))

    for i, glob_file in enumerate(files_path):
        #print(glob_file)
        epoch_filename, file_ext = extract_epoch(glob_file) # 1623432424.jpg
        fractional = 'False' if have_fractional == 'True' else 'True'
        yyyymmdd_name = tim1.epoch_to_yyyymmdd(epoch_filename, have_prefix=have_rcwy_id, strip_fractional=fractional) # 2021-09-17 11:07:09.jpg
        #print(yyyymmdd_name, epoch_ext)
        #print(glob_file)
        #print(epoch_filename+file_ext)
        #print(yyyymmdd_name+file_ext)
        replace_file = replace_filename(glob_file, epoch_filename+'.jp'+file_ext, yyyymmdd_name+'.jp'+file_ext)
        #print(replace_file)
        os.rename(glob_file, replace_file) # .../.../.../1623432424.jpg --> .../.../.../20210917_1107.jpg
        
        print("processed file: ", i, glob_file)


