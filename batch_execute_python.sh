#!/bin/bash 
########################################################################################
# Always remember to backup!!! Always remember to backup!!! Always remember to backup!!! 
########################################################################################

# This script execute python script with varied input arguments
# the following for loop in this script is equivalent to run, sequentially:
#
# python epochname_to_yyyymmdd.py file_path='./images/1_1/*' have_prefix=False have_surffix=True
# python epochname_to_yyyymmdd.py file_path='./images/2_1/*' have_prefix=False have_surffix=True
# ...
# python epochname_to_yyyymmdd.py file_path='./images/102_1/*' have_prefix=False have_surffix=True

for i in {1..102}
do 
	python epochname_to_yyyymmdd.py file_path='./images/'"$i"'_1/*' have_prefix=False have_surffix=True
done 
