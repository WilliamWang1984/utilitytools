# Generate random noise RGB images

import numpy as np
import cv2
import argparse
'''
from Profiler import Profiler

prof = Profiler()

@prof.timeit
#@prof.ramit
def dummy_test_function(test, **kwargs):
    print(test)
    print('dummy_function wrapped by ramit then timeit!')
'''
ap = argparse.ArgumentParser()
ap.add_argument("-n", "--number", required=True, help="# of noise RGB images required")
ap.add_argument("-x", "--width", required=True, help="image width")
ap.add_argument("-y", "--height", required=True, help="image height")
ap.add_argument("-t", "--type", default='.jpg', help="generated image type, default jpg")
ap.add_argument("-m", "--name", default='rgbNoise', help="image name pattern, default rgbNoise")
args = vars(ap.parse_args())
'''
logs = {}
new_time_format = '%3.1f'
new_ram_format = '%4.4f'
dummy_test_function('test me for anything!!!', log_name=logs, time_format=new_time_format, ram_format=new_ram_format)

print(logs)


'''
for i in range(args["number"]):
	random_rgb = np.random.randint(255, size=(args["height"], args["width"], 3), dtype=np.uint8)
	cv2.imwrite(args["name"]+str(i)+args["type"], random_rgb)

