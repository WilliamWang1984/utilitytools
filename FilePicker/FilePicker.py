import os
from glob import glob
import argparse

class FilePicker:

    def __init__(self, src_path='./images', dst_path='./images/picked', interval=10):
        self.src_path = src_path
        self.dst_path = dst_path
        self.interval = interval

    def move_picked_files(self, picked_file_list, target_path):
        if not os.path.exists(target_path):
            os.mkdir(target_path, mode = 0o755)

        for pick_i in picked_file_list:
            filename = pick_i.split('/')[-1]
            os.rename(pick_i, target_path+filename)

    def get_picked_files(self, ls_all_files, k):
        return ls_all_files[::k]

if __name__ == '__main__':

    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--src_path", type=str, default="./images/*.jpg", help="Path to source image files")
    ap.add_argument("-q", "--dst_path", type=str, default="./images/picked/", help="Path to destination image files")
    ap.add_argument("-n", "--interval", type=int, default=10, help="Pick first file in every <interval> files")
    args = vars(ap.parse_args())
    
    fp1 = FilePicker(args["src_path"], args["dst_path"], args["interval"])

    ls_sorted_all = sorted(glob(args["src_path"]))
    
    #print(ls_sorted_all)

    picked_list = fp1.get_picked_files(ls_sorted_all, args["interval"])

    #print(picked_list)

    fp1.move_picked_files(picked_list, args["dst_path"])
