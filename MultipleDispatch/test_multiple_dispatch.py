from MultipleDispatch import MultipleDispatch

if __name__ == '__main__':

    print("Testing multiple dispatch: ")

    md1 = MultipleDispatch()
    
    default_12 = md1.add()
    print(default_12)
    
    int_12 = md1.add(1, 2)
    print(int_12)

    float_12 = md1.add(1.0, 2.0)
    print(float_12)

    str_12 = md1.add("1", "2")
    print(str_12)

    #md1.add(1, 2.0)
 
