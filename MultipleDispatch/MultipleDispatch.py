from multipledispatch import dispatch

class MultipleDispatch:

    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y

    @dispatch()
    def add(self, x=None, y=None):
        print("Dispatched to default")
        return x and y

    @dispatch(int, int)
    def add(self, x=1, y=2):
        print("Dispatched to int add")
        return int(x + y)

    @dispatch(float, float)
    def add(self, x=1.0, y=2.0):
        print("Dispatched to float add")
        return float(x + y)

    @dispatch(str, str)
    def add(self, x='1', y='2'):
        print("Dispatched to string add/concatenate")
        return str(x + y)
