import re

#regExp = r"^([4-5][2-9])([0-9]{9})"
regExp = r"(4200000000[1-9]|420000000[1-9][0-9]|42000000[1-9][0-9]{2}|4200000[1-9][0-9]{3}|420000[1-9][0-9]{4}|42000[1-9][0-9]{5}|4200[1-9][0-9]{6}|420[1-9][0-9]{7}|42[1-9][0-9]{8}|4[3-9][0-9]{9}|50000000000)"

testcase0 = '0'
testcase1 = '10000000000'
testcase2 = '42000000000'
testcase3 = '42000000001'
testcase4 = '45000000000'
testcase5 = '49999999999'
testcase6 = 'wafwefasd-50000000000-asfeasdf'
testcase7 = '0000000000000'
testcase8 = '100000000000'
testcase9 = '0000000000001'
testcaseA = '00000000001'
testcaseB = '41999999999'
testcaseC = '50000000001'
testcaseD = '458asdfasdf'

mo0 = re.search(regExp, testcase0)
print(mo0.group(), mo0.group() == testcase0) if mo0 is not None else print(None)
mo1 = re.search(regExp, testcase1)
print(mo1.group()) if mo1 is not None else print(None)
mo2 = re.search(regExp, testcase2)
print(mo2.group()) if mo2 is not None else print(None)
mo3 = re.search(regExp, testcase3)
print(mo3.group(), mo3.group() == testcase3) if mo3 is not None else print(None)
mo4 = re.search(regExp, testcase4)
print(mo4.group()) if mo4 is not None else print(None)
mo5 = re.search(regExp, testcase5)
print(mo5.group()) if mo5 is not None else print(None)
mo6 = re.search(regExp, testcase6)
print(mo6.group()) if mo6 is not None else print(None)
mo7 = re.search(regExp, testcase7)
print(mo7.group()) if mo7 is not None else print(None)
mo8 = re.search(regExp, testcase8)
print(mo8.group()) if mo8 is not None else print(None)
mo9 = re.search(regExp, testcase9)
print(mo9.group()) if mo9 is not None else print(None)
moA = re.search(regExp, testcaseA)
print(moA.group()) if moA is not None else print(None)
moB = re.search(regExp, testcaseB)
print(moB.group()) if moB is not None else print(None)
moC = re.search(regExp, testcaseC)
print(moC.group()) if moC is not None else print(None)
moD = re.search(regExp, testcaseD)
print(moD.group()) if moD is not None else print(None)






