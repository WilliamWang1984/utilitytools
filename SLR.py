import numpy as pn
import cv2
import time

#capString = 0
# 0 
# 1
capString = 1
# or
#capString = 'http://<your_wificam_name>:<your_wificam_pass>=@<your_wificam_ip>:8080/stream/video/mjpeg'

cap = cv2.VideoCapture(capString)
t00 = time.time()
while True:
    _, frame = cap.read()
    cv2.imshow('frame', frame)
    k = cv2.waitKey(5) & 0xFF
    t01 = time.time()
    print("{} fps, size: {}".format(int(1./(t01 - t00)), frame.shape))
    t00 = time.time()
    if k == 27:
        break
    if k == ord('r'):
        cv2.imwrite(str(t00)+'.jpeg', frame)
