# Courtesy of:
# https://stackoverflow.com/questions/2280334/shortest-way-of-creating-an-object-with-arbitrary-attributes-in-python
# https://stackoverflow.com/questions/2511222/efficiently-generate-a-16-character-alphanumeric-string

# A python class whose instances can accept the creation of any arbitrary property names
# Univeral Unique ID python library is used to create arbitrary property names
import time
import uuid
from multiprocessing import Pool

class Arbitrary:
    def __init__(self, **kwargs):
        self.my_kwargs = kwargs
        self.__dict__.update(kwargs)

    ### Notice the differences when calling myfunc with and without the @property wrapper
    ### i.e. with @property wrapper, the my_kwargs variable is {}, the dictionary values are None
    ### without @property wrapper, the my_kwargs are of <bound method Arbitrary.myfunc of <__main__.Arbitrary object at 0x#######>>
    #@property
    def myfunc(self):
        print(self.my_kwargs)

    def mysum(self, x, y):
        #print("x, y: (%d, %d)" % (x, y))
        return x * 10 + y * 100

    def mysum_tuple_in(self, z):
        return z[0] * 10 + z[1] * 100

if __name__ == '__main__':
    arb = Arbitrary()
    
    def _parArbFuncs(z):
        return z[0] * 10 + z[1] * 100

    listofArbitraryProperties = []
    for i in range(10):
        uidv4 = uuid.uuid4()
        setattr(arb, uidv4.hex, i)
        listofArbitraryProperties.append(uidv4.hex)
    
    #print(arb)

    print('Now printing a list of all attributes of the initialized Arbitrary Class instance:\n')
    for j in dir(arb):
        print(j, ':\t', getattr(arb, j, 'Default'))
    
    print('\n----------------------------------------------------------\n')

    print('Now printing a list of uuid attributes of the initialized Aribitrary Class instance:\n')
    for k in listofArbitraryProperties:
        print(k, ':\t', getattr(arb, k, 'Default if no property (attribute)'))

    print('\n----------------------------------------------------------\n')

    timer0 = time.time()
    listofArbitraryFunctions = []
    for l in range(100000):
        uidv4 = uuid.uuid4()
        #setattr(arb, uidv4.hex, {'expr':"+", 'args':[l+123, l+246]})
        setattr(arb, uidv4.hex, arb.mysum_tuple_in((l+123, l+321)))
        listofArbitraryFunctions.append(uidv4.hex)
    
    
    print('Now printing a list of uuid functions of the initialized Aribitrary Class instance:\n')
    for m_ind, m in enumerate(listofArbitraryFunctions):
        print(m, ':\t', getattr(arb, m, 'Default if no function'))
    

    timer1 = time.time()

    print('Time cost seq is %f' % (timer1-timer0))

    argsList1 = [(x+123) for x in list(range(100000))] 
    argsList2 = [(y+321) for y in list(range(100000))]
    argsList = list(zip(argsList1, argsList2))
    #print(list(argsList))
    
    timer2 = time.time() 

    with Pool(4) as pl:
        #pl.map(arb.mysum_tuple_in, argsList)
        print(pl.map(arb.mysum_tuple_in, argsList))
    #    print(pl.map(_parArbFuncs, argsList))
    
    timer3 = time.time()

    print('Time cost par is %f' % (timer3-timer2))

    print('\n----------------------------------------------------------\n')

    print('Time cost (seq - par) is %f' % ((timer1-timer0)-(timer3-timer2)))

