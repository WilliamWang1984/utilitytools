# Courtesy of:
# https://stackoverflow.com/questions/34007028/efficient-way-of-computing-kullback-leibler-divergence-in-python
# https://towardsdatascience.com/kl-divergence-python-example-b87069e4b810

import numpy as np
import scipy.stats as sc

import random

class KLDivergence:
    def __init__(self):
        pass

    '''
    Input distributions are p and q of python lists
    '''
    def pair_kld(self, p, q):
        # convert lists p, q to numpy arrays
        np_p = np.array(p)
        np_q = np.array(q)
        return np.sum(np.where(np_p != 0, np_p * np.log(np_p / np_q), 0))


    '''
    Input distributions are organized in matrix of d, each row represent
    a distribution
    '''
    def n_kld(self, d):
        n = d.shape[0]
        m = np.zeros((n, n))
        for i in range(0, n):
            for j in range(0, n):
                if (i != j):
                    m[i, j] = sc.entropy(d[i, :], d[j, :])
        return m

    def rand_list(self, n):
        return random.sample(range(0, 100), 5)

if __name__ == '__main__':
    kld1 = KLDivergence()
    sp = kld1.rand_list(5)
    sq = kld1.rand_list(5)
    spFloat = np.random.rand(1, 5)
    sqFloat = np.random.rand(1, 5)
    dFloat = np.random.rand(5, 10)
    
    print(sp, sq, spFloat[0], sqFloat[0], dFloat)
    
    pq1 = kld1.pair_kld(sp, sq)
    nppq1 = kld1.pair_kld(spFloat[0], sqFloat[0])
    
    print(pq1, nppq1)

    n2 = kld1.n_kld(dFloat)
    print(n2)

