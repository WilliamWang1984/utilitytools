# This repo contains multiple Python and Bash scripts to handle image processing and file operations.

## Frequently used bash commands
1. To list all files under current folder
```bash
ls
```
2. Count number of files
```bash
ls -1 | wc -l
```
3. List into a txt file
```bash
ls > list.txt
```
4. Copy, Move / Rename
```bash
cp file1.txt file2.txt
mv file1.txt file2.txt
```
5. Check processes
```bash
ps aux | grep <process_name (e.g. python3)>
```
6. Check parental processes of defunct processes
```bash
ps -ef | grep defunct
```
7. Kill process with maximum privilage
```bash
sudo kill -9 <process_id>
```

## Argparser
Define a class whose instance enables the comprehension of arguments in two modes: the class instance initialization mode and the sys.argv passed-in mode. Positional arguments have keys defined as 'position_k'. The output is a list of tuples of (key, value) pairs.

## BatchRename
Batch Renaming scripts to append renaming pattern before (prefix) or after (surfix) original filename, note in the latter case the pattern is appended before the file extended name but after the file base name. 

## BlogPosts
Interesting readings 

## CopyFiles
## copy_from_list.sh
The script copy files to parent level folder, according to the text file that listed. Alternatively the keyword 'cp' may be replaced by 'mv' to achieve cut instead of copy 
Usage:
```bash
sh copy_from_list.sh ToCopy.txt
```

## Cropper
Define a class to crop, mask and padding images, save and stack cropped image patches

## EmailDiskSpace
Send Gmail when disk space goes low

## FilePathSplitter
Define a class to process file path with predefined and passedin delimiters

## FilePicker
Select one file out of every N from a list of files

## games
A list of interesting games written in python

## ImageFormatConverter
Use Pillow to convert images of one format (e.g. .pgm) to another (e.g. .jpg / .png / .tiff)

## JsonFlattening
These scripts nested under this folder employs two approaches to convert a deeply nested json to its flat format. See the relevant article in BlogPosts subfolder.
## flatten_json_iteratively.py

## flatten_json_recursively.py

## LabelConverter
Define a class to convert txt labels to remove, update, split, merge, redefine and convert from xyxy2xywh or reverse

## MonteCarloMethods
Monte carlo simulations of PI approximation

## MoveFiles
## move_files.sh
The script moves all images in subfolders of folder 'moveParentDirectory' to directly under the 'moveParentDirectory' itself, appending subfolder names to the original image name as prefix

## /ndtest
Github Repo to compute the KSTest for two dimensional correlated data, that is: x, y coordinates are correlated, not iid (independently identical distributed), referring to 'Two-Dimensional Goodness-of-Fit Testing in Astronomy' by Peacock, J.A. 1983.
Usage:
Refer to the comments in ndtest.py by the original author for more explaination of arguments and return values, code segments started on and after if __name__ == '__main__': is referer's work 


## NuclearBomb
A dangerous bash script (essentially perl) to empty everything no matter how many entities are there, use with caution

## PowerfulLambdas
Lambda functions to implement Fibonacci, Factorial and Quicksort

## Profiler
## Profiler.py + dummytest_profiler.py
Measure python function run time and run mem
To use, simply copy Profiler.py to your script folder and import Profiler class into your python script by 'from Profiler import Profiler', then create Profiler class instance by 'profiler1 = Profiler()', finally use the @profiler.timeit and @profiler1.ramit decorator syntax above your python function's definition
Usage:
```bash
python dummytest_profiler.py
```

## ptpy
nikon.py

## RemoveFiles
## remove_from_list.sh
The script remove files according to the text file that listed to delete
Usage:
```bash
sh remove_from_list.sh ToRemove.txt
```
## remove_from_list.py
Remove files according to the text file that listed to delete
Usage:
```bash
python remove_from_list.py -i ToRemove.txt
```
## list_extra_files.sh
The script compares listA.txt with listB.txt, notes down - for every line in listA.txt but not in listB.txt - in listC.txt 

## setpin_UploadToArduino
Simple arduino pin write based on read 

## TakePicture
A serials of scripts to 1. turn on arduino pin, 2. take picture, 3. turn off arduino pin
## gphoto2_image.sh
This script captures images from the SLR and save them as using capture time as file name.
## gphoto2_movie.sh
This script streams video from the SLR, need to use in combination with SLR.py to save the video frame into computer.

## TextNumpyer
Read and write txt to numpy, skip empty txt file

## Timer
Define a class to convert time formats and the name of files named in different time formats, slice time into intervals, shift slices forward and backward. 

## Arbitrary.py
Define a class whose instances is capable of accepting any arbitrary attributes (properties) definitions and assignments. Arbitrary attributes are generated using the python uuid (Unique Universal Identifier) library.  

## batch_execute_python.sh
This script batch calls python / python3 executions, with the called calling python script's input arguments updates every iteration in the for loop. 

## batch_remove.sh
This script removes folders and files in the same directory with it, according to the patterns defined as 'folder_to_remove_N' and 'file_to_remove_N', where N is the numerical element of the batch of folders / files.

## batch_resize.sh 
This script is used together with resize_files.sh for handling resize of all images in multiple subfolders. Both files should be put into the same level as the multiple subfolders that contains images.

## compress_folder.sh
The script is used to zip all folders it sees into .zip compression files.

## crop_rois.py
Taking input image files patch, crop them into predefined patches and save into the specified destination folder. 
Usage:
```bash
python crop_rois.py -i <Input_Path> -o <Output_Path>
```

## decompose_files.sh
The script is used to convert RGB image to mono color R, G or B images. Remember to backup before running.

## Discoverer.py
Define a class to discover Advantech Adam controller ip address from given MAC address

## docker-save-running-ps.sh
Save a running docker container of given name

## dump_operations.py
Checking frozen .pb graph structure in terminal. 
Usage:
```bash
python dump_operations.py frozen_graph.pb
```

## Excel.py
A few functions to convert between integer column indices and excel style column indices

## extract_and_stitch.py
Extracting images from multiple-level subfolders, see comments in the script.
Usage:
```bash
python extract_and_stitch.py
```

## filter_files.sh
This script is used to move image files smaller (in terms of kilobyte filesize on harddrive) than set value to designated folder.

## Graph.py
Graph minimal spanning tree computations

## KLDivergence.py
Class function to compute the Kullback-Leibler Divergence between two distributions with support to two types input formats: p, q format (a pair of distributions) and n format (an array of n distributions, one row each).
Usage:
```bash
python KLDivergence.py
```

## KSTest.py
Class to compute the Kolmogorov–Smirnov test, for one dimensional data only, for 2D data points, use the repo in /ndtest
Usage:
```bash
python KSTest.py
```

## Monitor.py
Script to grab current systems monitor info

## Networker.py
A few simple internetworking functions suitable for Linux OS systems

## pickle_view.py
View contents of a pickle file
Usage:
```bash
python pickle_view.py pickle_file.pkl
```

## qr_reader.py
qr code scanner script based on serial ttyACM\* connection

## randint2rgb.py
Generate random noise RGB images with uniform sampling (0~255). Also made available in Painter class. 
Usage:
```bash
python randint2rgb.py -n <Number_of_Images> -x <Image_Width> -y <Image_Height>
```

## randomized_color_assigner.py
Generate a list of random colors w.r.t input file as a list of class names / IDs
```bash
python randomized_color_assigner.py -i <list_of_classes.names>
```

## README.md
This README file

## reorganize.py
Reorganize a overly grown very large directory containing 100000+ files into reasonably smaller chunks of sub directories each containing the specified n files for easy browsing

## resize_files.sh
This script is used standalone to resize all images within the same folder. The script should be put to the same level as the images to resize.

## rgb2unique.py
Convert RGB image to uniquely valued Graylevel image, the resulting gray pixel intensity is given by G = 4*B + 2*G + 1*R, such [4, 2, 1] (which are 2^2, 2^1, 2^0) can be adjusted.
Usage:
```bash
python rgb2unique.py -i <Input_Image.jpg>
```

## set_title.fun
Set ubuntu command line terminal title

## SLR.py
The script streams video from external camera using simple opencv functionality

## split_files.sh
This script is used to split a subfolder containing huge amount of files (e.g. 1000000) into subfolders containing predefined number (e.g. 2000) of files. Two versions of implementation are included. 

## usb_find.sh
The script finds a list of serial USB communication devices utilizing bash. More details in the comment section of the script

## usb_scanner_read_demo.py
Script that reads multiple usb scanner devices, using the PyUSB lib. Conversion functions for different scanner types (CDC vs HID) are also provided.
