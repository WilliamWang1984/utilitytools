#!/bin/bash
# courtesy of https://askubuntu.com/questions/596489/how-to-delete-files-listed-in-a-text-file
# Usage:
# sh remove_from_list.sh list.txt
# Backup before removal !

if [ -z "$1" ]; then
    echo -e "Usage: $(basename $0) FILE\n"
    exit 1
fi

if [ ! -e "$1" ]; then
    echo -e "$1: File doesn't exist.\n"
    exit 1
fi

while read -r line; do
    [ -n "$line" ] && rm -- "$line"
done < "$1"
