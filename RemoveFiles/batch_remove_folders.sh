# Script to batch remove folders
# -- folder_pattern
# -- start_index
# -- end_index
#
#
# $ sh batch_remove_folders.sh -n <pattern>
# Remove all folders from: folder2remove_1 to folder2remove_100
# $ sh batch_remove_folders.sh -n 'folder2remove'

while getopts n: flag
do 
	case "${flag}" in
		n) namepattern=${OPTARG};;
	esac
done

echo "name_pattern": $namepattern;

for i in {1..5}
do 
	rm -rf "$namepattern"'_'"$i"'/' 
done 
