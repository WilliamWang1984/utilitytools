import os
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True, help="Input txt file name that lists all files to delete")
args = vars(ap.parse_args())

flist = open(args["input"])
for f in flist:
    fname = f.rstrip() # or depending on situation: f.rstrip('\n')
    # or, if you get rid of os.chdir(path) above,
    # fname = os.path.join(path, f.rstrip())
    if os.path.isfile(fname): # this makes the code more robust
        os.remove(fname)

# also, don't forget to close the text file:
flist.close()
