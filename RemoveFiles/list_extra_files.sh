# !bin/bash
# Courtesy of https://unix.stackexchange.com/questions/171113/bash-scripting-problem-compare-two-lists-and-create-a-third
# Find all files that listed in listA.txt but not in listB.txt, write them to listC.txt
# 'comm' is used to compare two sorted files line by line
# -1 supress column 1 (lines unique to FILE1)
# -2 supress column 2 (lines unique to FILE2)
# -3 supress column 3 (lines that appear in both files)
# e.g. comm -23 (ignore lines unique to FILE2, and in both files)
# 
# Modify the operated files listA.txt, listB.txt and listC.txt accordingly

#comm -23 listA.txt listB.txt > listC.txt
comm -23 frms.txt imgs.txt > frm-imgs.txt
