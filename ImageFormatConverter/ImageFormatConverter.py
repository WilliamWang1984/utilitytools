import os
from glob import glob
from PIL import Image
import argparse

import cv2
import numpy as np

class ImageFormatConverter:

    def __init__(self, from_format='pgm', to_format='jpg'):
        self.from_format = from_format
        self.to_format = to_format

    
    # Conversion from OpenCV (numpy, b, g, r) to Pillow (R, G, B)
    def opencv_to_pillow(self, input_img):
        img = cv2.cvtColor(input_img.copy(), cv2.COLOR_BGR2RGB)
        output_img = Image.fromarray(img)

        return output_img


    # Conversion from Pillow (RGB) to OpenCV (b, g, r)
    def pillow_to_opencv(self, input_img):
        output_img = np.asarray(input_img.copy())
        
        return output_img


    # Conversion from RGB to Gray
    def rgb_to_gray(self, input_img):
        output_img = cv2.cvtColor(input_img, cv2.COLOR_BGR2GRAY)

        return output_img


    # Conversion from Gray to Pseudo RGB
    def gray_to_rgb(self, input_img):
        output_img = np.stack((input_img)*3, axis=-1)

        return output_img


if __name__ == '__main__':
    
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--src_path", type=str, default="./images/*.", help="Path to source image files")
    ap.add_argument("-q", "--dst_path", type=str, default="./images/output/", help="Path to destination image files")
    ap.add_argument("-f", "--from_format", type=str, default="pgm", help="Image source format")
    ap.add_argument("-t", "--to_format", type=str, default="jpg", help="Image destination format")
    args = vars(ap.parse_args())

    ifc1 = ImageFormatConverter(from_format=args["from_format"], to_format=args["to_format"])

    ls_imgs = glob(args["src_path"]+ifc1.from_format)

    if not os.path.exists(args["dst_path"]):
        os.mkdir(args["dst_path"], mode = 0o755)

    for img in ls_imgs:
        img_pgm = Image.open(img)
        img_name = img.split('/')[-1]

        new_name = args["dst_path"]+img_name.replace(ifc1.from_format, ifc1.to_format)
        img_pgm.save(new_name)

