#!/bin/python

import numpy as np
from multiprocessing import Pool

PRECISION = 0.1

class Flat_rectangle_problem:

    #-----------+-----------+-----------+-----------+-----------+      d_high
    #           |           |           |           |           |
    #           |           |           |           |           |
    #        +--+-----------+----+      |           |           |
    #        |  |           |    |      |           |           |
    #        |  |           |    |      |           |           |
    #--------+--+-----------+----+------+-----------+-----------+
    #        +--+-----------+----+      |           |           |
    #           |           |           |           |           |
    #           |           |           |           |           |      # 6intersection-rectangle
    #           |           |           |           |           |
    #           |           |  +--------+---------+ |           |
    #-----------+-----------+--|--------+---------|-+-----------+
    #    +------+----------+|  |        |         | |           |                 
    #    |      |          ||  |        |         | |           |                           
    #    |      |          ||  +--O-----+---------+ |           |                      # 4intersection-rectangle
    #    |      |          ||           |           |           |                             (Origin)
    #    +------+----------+|           |           |           | # 2intersection-rectangle
    #-----------+-----------+-----------+---_-+-_---+-----------+
    #           |           |           | +       + |           |
    #           |           |           |/         \|           |
    #           |           |           +           +           |
    #           |           |           |\         /|           |                       # unit-circle intersects four times regardless of position
    #           |           |           | + _   _ + |           |
    #-----------+-----------+-----------+-----+-----+-----------+
    #           |           |           |           |           |
    #           |           |           |           |           |
    #           |           |           |           |           |
    #           |           |           |           |           |
    #           |           |           |           |           |
    #-----------+-----------+-----------+-----------+-----------+     d_low
    #
    # d_low ................................................d_high

    # 4 * #_unit-circles (A) == Total # of intersections of all circles with grid
    # 2 * #_2intersection_rectangle + 4 * #_4intersection_rectangle + 6 * #_6intersecction-rectangle (B) == Total # of intersections of all rectangles with grid
    # Presumbly (A == B) == Total # of intersctions of a particular shape bearing same area (S) with unit-circle

    def __init__(self,r,n,m,d):
        
        self.d_low = r * d[0]
        self.d_high = r * d[1]
        self.r = r # length for the short side of the rectangle, half of square grid's unit-square's length, equivalent to the radius of the unit-length circle

        self.n = n  #no of throws
        self.m = m  #no of simulations


    def samples(self, pi):

        # generate x0, y0 between d_low and d_high
        x0y0 = np.random.uniform(low=self.d_low, high=self.d_high, size=(2, 1))
        x0 = x0y0[0][0]
        y0 = x0y0[1][0]
        
        # compute x1, y1 for x0, y0
        x1 = x0 + pi * self.r
        y1 = y0 + self.r
        
        # compute intersections for (x0, y0, x1, y1) and r grid
        
        # from x0 to x1, count number of vertical lines:
        # if x0 in left half of grid cell, two intersections
        # elif x0 in right half of grid cell, four intersections        
        
        (a, b) = divmod(x0, self.r) 
        xx = 4 if 2 * b > self.r else 2

        # from y0 to y1, count number of horizontal lines:
        # if y0 in upper half of grid cell, no intersection
        # elif y0 in lower half of grid cell, two intersections
        
        (c, d) = divmod(y0, self.r)
        yx = 0 if 2 * d > self.r else 2

        nx = xx + yx

        # four corners of rectangles, plus number of intersections
        return [x0, y0, x1, y1, nx]


    def simulation(self):
        # m simulation

        arr_pi = []
        for j in range(self.m):
            
            min_diff = 4 * self.n
            best_pi = 4

            for pi in np.arange(2, 4, PRECISION):

                # n throw
                hits = 0 #setting the success to 0
                for i in range(self.n):
                    [x0, y0, x1, y1, nx] = self.samples(pi)
                    hits += nx
                
                now_diff = abs(4 * self.n - hits)

                if now_diff < min_diff:
                    min_diff = now_diff
                    best_pi = pi
            
            arr_pi.append(best_pi)
        
        # approximations of pi    

        # 1. the total number of hits == 4 * #_of_throw
        # 2. the area of the rectangle == the area of unit-circle == Pi * r * r
        # 3. rectange height == r
        # 4. rectangle width == Pi * r
        # 5. given 4 * #_throw, iterate value for estimated Pi from 2 to 4, do #_throw in each iteration, find the Pi that gives the closest #_intersections to 4 * #_throw
        
        print(arr_pi)
        return arr_pi

# numberof_runs, numberof_rects per run, rect height (1/2 grid size)
y = Flat_rectangle_problem(5, 10000, 5, [-10, 10])

#with Pool(8) as p:
#    p.map(y.simulation())
l_pi = y.simulation()

#print(l_pi)
print(np.mean(l_pi))
