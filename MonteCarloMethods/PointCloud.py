import random
import math
import numpy as np
import argparse 

# B = 0.001

class PointCloud:

    def __init__(self, side_length):
        
        self.side_length = side_length
        self.radius = side_length / 2.0 


    def gen_random(self):

        return random.uniform(-self.radius, self.radius)


    def gen_xy(self):

        def _gen_random():
        
            return np.random.uniform(low=-self.radius, high=self.radius, size=(2, 1))
       
        x = _gen_random()

        return (x[0][0], x[1][0])


    def check_dot_pos(self, dist):

        return np.sign(dist-self.radius)


    def gen_dot_2d(self):
        
        (x, y) = self.gen_xy()

        dist =  math.sqrt(x * x + y * y)
        dot_pos = self.check_dot_pos(dist)
        
        #if (x >= -self.radius-B and x < -self.radius+B or x >= self.radius-B and x < self.radius+B) or (y >= -self.radius-B and y < -self.radius+B or y >= self.radius-B and y < self.radius+B):
        #    dot_pos = 2

        #if x == -self.radius or x == self.radius or y == -self.radius or y == self.radius:
        #    dot_pos = 2

        return (dot_pos, dist, x, y)


    def gen_dot_3d(self):

        (x, y) = self.gen_xy()
        z = self.gen_random()
        
        dist = math.sqrt(x * x + y * y + z * z)
        dot_pos = self.check_dot_pos(dist)
        
        #if (x >= -self.radius-B and x < -self.radius+B or x >= self.radius-B and x < self.radius+B) or (y >= -self.radius-B and y < -self.radius+B or y >= self.radius-B and y < self.radius+B) or (z >= -self.radius-B and z < -self.radius+B or z >= self.radius-B and z < self.radius+B):
        #    dot_pos = 2

        #if x == -self.radius or x == self.radius or y == -self.radius or y == self.radius or z == -self.radius or z == self.radius:
        #    dot_pos = 2

        return (dot_pos, dist, x, y, z)


    def count_dots(self, n):

        n_in_2d = 0
        n_out_2d = 0
        n_in_3d = 0
        n_out_3d = 0

        for i in range(0, n):

            (p2d, d2d, x2d, y2d) = self.gen_dot_2d()
            (p3d, d3d, x3d, y3d, z3d) = self.gen_dot_3d()

            if p2d == -1:
                n_in_2d += 1
            elif p2d == 1:
                n_out_2d += 1

            if p3d == -1:
                n_in_3d += 1
            elif p3d == 1:
                n_out_3d += 1
     
        return n_in_2d, n_out_2d, n_in_3d, n_out_3d

if __name__ == '__main__':
    
    ap = argparse.ArgumentParser()
    ap.add_argument('-n', '--n_dots', type=int, default=100000, help='number of random dots to generate')
    args = vars(ap.parse_args())

    mc1 = PointCloud(10)
    
    n_in_2d, n_out_2d, n_in_3d, n_out_3d = mc1.count_dots(args['n_dots'])
   
    print("2D Monte Carlo for Pi / 4 (area of circle in unit length square, 0.7853981633974483)")
    print(n_in_2d / (n_in_2d + n_out_2d))

    print ("3D Monte Carlo for Pi / 6 (volumn of sphere in unit length cubic, 0.5235987755982988)")
    print(n_in_3d / (n_in_3d + n_out_3d))
    
