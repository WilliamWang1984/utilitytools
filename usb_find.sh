# bash script finding a list of usb devices and return only serials
# consider this an USB hardware api for serial USB sensors
# 
# In code (default Python), do the following:
# ```
# import os
# import serial
# devices_to_use = []
# stream = os.popen('bash usb_find.sh')
# lines = stream.readlines()
# for line in lines:
#     if 'device_identifier' in line and 'connection_identifier(tty)' in line:
#         devices_to_use.append(serial.Serial(line[0:12]))
# 
# for device in devices_to_use:
#     print(device.readline())
# ```
#!/bin/bash

#Devices which show up in /dev have a dev file in their /sys directory. So we search for directories matching this criteria.
for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
    (
        #We want the directory path, so we strip off /dev.
        syspath="${sysdevpath%/dev}"

        #This gives us the path in /dev that corresponds to this /sys device.
        devname="$(udevadm info -q name -p $syspath)"

        #This filters out things which aren't actual devices. Otherwise you'll get things like USB controllers & hubs.
        [[ "$devname" == "bus/"* ]] 

        #The udevadm info -q property --export command lists all the device properties in a format that can be parsed by the shell into variables. 
        #So we simply call eval on this. 
        #This is also the reason why we wrap the code in the parenthesis, so that we use a subshell, and the variables get wiped on each loop.
        eval "$(udevadm info -q property --export -p $syspath)"
        
        #More filtering of things that aren't actual devices.
        [[ -z "$ID_SERIAL" ]]

        #echo the path and id
        echo "/dev/$devname - $ID_SERIAL"
    )
done
