class Excel:
    def __init__(self):
        self.debug = False

    def int_to_excel_column_string(self, n):
        string = ""
        while n > 0:
            n, remainder = divmod(n - 1, 26)
            string = chr(65 + remainder) + string
        return string


    def excel_column_string_to_int(self, excelColStr):
        intIndex = 0
        for c in excelColStr:
            if c in string.ascii_letters:
                intIndex = intIndex * 26 + (ord(c.upper()) - ord('A')) + 1
        return intIndex

