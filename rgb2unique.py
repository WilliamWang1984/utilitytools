# Convert RGB image to gray level of unique pixel intensities
# the uniqueness of pixels are determined by weighting coefficients
# 
import cv2
import numpy as np
from PIL import Image
import scipy.misc
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="path to input RGB image")
args = vars(ap.parse_args())

img = cv2.imread(args["image"])
arr = [4, 2, 1]

# cv2 reads, by default, B[:,:,0], G[:,:,1], R[:,:,2]
imgBlue = arr[0]*img[:,:,0]
imgGreen = arr[1]*img[:,:,1]
imgRed = arr[2]*img[:,:,2]

matCold = np.add(imgBlue, imgGreen)
matUnique = np.add(matCold, imgRed)

#imgUnique = Image.fromarray(matUnique)
#imgUnique.show()

A = np.double(matUnique)
out = np.zeros(A.shape, np.double)
normalized = cv2.normalize(A, out, 1.0, 0.0, cv2.NORM_MINMAX)

cv2.imshow('channel_weighted_gray', normalized)

#imgGray = scipy.misc.imread('example.jpg', mode='L')
imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
cv2.imshow('linear_combined_gray', imgGray)
cv2.waitKey(0)
