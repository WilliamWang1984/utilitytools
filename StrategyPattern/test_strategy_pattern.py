from abc import ABC, abstractmethod

class Car():
    """
    Base class for car types
    """
    def __init__(self, cruise_control_strategy=None, drive_strategy=None, moon_roof_strategy=None):
        self._cruise_control_strategy = cruise_control_strategy
        self._drive_strategy = drive_strategy
        self._moon_roof_strategy = moon_roof_strategy

    @property
    def cruise_control_strategy(self):
        return self._cruise_control_strategy

    @cruise_control_strategy.setter
    def cruise_control_strategy(self, cruise_control_strategy):
        self._cruise_control_strategy = cruise_control_strategy

    @property
    def drive_strategy(self):
        return self._cruise_control_strategy

    @drive_strategy.setter
    def drive_strategy(self, drive_strategy):
        self._drive_strategy = drive_strategy

    @property
    def moon_roof_strategy(self):
        return self._moon_roof_strategy

    @moon_roof_strategy.setter
    def moon_roof_strategy(self, moon_roof_strategy):
        self._moon_roof_strategy = moon_roof_strategy

    def cruise_control(self):
        self._cruise_control_strategy.cruise_control()

    def drive(self):
        self._drive_strategy.drive()

    def moon_roof(self):
        self._moon_roof_strategy.moon_roof()

class CruiseControlBehavior(ABC):
    """
    Defining interface for cruise control functionality
    """
    @abstractmethod
    def cruise_control(self):
        pass

class AdaptiveCruiseControl(CruiseControlBehavior):
    """
    Algorithm #1: adaptive cruise control.
    Used by: Mazda and Audi
    """
    def cruise_control(self):
        print("Adaptive cruise control")

class SimpleCruiseControl(CruiseControlBehavior):
    """
    Algorithm #2: simpe cruise control.
    Used by: BMW
    """
    def cruise_control(self):
        print("Adaptive cruise control")

class SelfDrive(CruiseControlBehavior):
    """
    Algorithm #3: Self driving car
    Used by: Tesla
    """
    def cruise_control(self):
        print("Self driving car")

class Mazda(Car):
    """
    class Mazda can directly call the method in the base class
    """
    def cruise_control(self):
        super().cruise_control()

if __name__ == '__main__':
    """
    main function which call the cruise control function for differnt car types
    """
    context = Car(AdaptiveCruiseControl())
    context.cruise_control()
    context.cruise_control_strategy = SelfDrive()
    context.cruise_control()
    m = Mazda(AdaptiveCruiseControl())
    m.cruise_control()
