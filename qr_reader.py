import serial
import time
import threading
import os
import sys


#Note: device permissions:
# 1. sudo usermod -a -G dialout $USER
# 2. restart

'''
Find usb port for QR scanner
'''
scanners = []
stream = os.popen('bash usb_find.sh')

lines = stream.readlines()
print('Lines: {}'.format(len(lines)))
for line in lines:
    if 'Symbol' in line and 'tty' in line:
        scanners.append(serial.Serial(line[0:12]))


#if no scanners, exit
if len(scanners) == 0:
    print('No devices...')
    sys.exit()


'''
At least one scanner found
'''
#ovarall timer
ot1 = time.time()

def thread_function(name, port):
    print("Thread {}: starting".format(name))
    #inner timer
    it1 = time.time()
    while True:
        raw = port.readline()
        #dec = raw.decode("utf-8")
        print(len(raw))
        print(raw)
        print(time.time() - it1)
        

'''
MAIN
'''
if __name__=='__main__':
    #init threads
    for i, scanner in enumerate(scanners):
        #assign thread
        thr = threading.Thread(target=thread_function, args=(i+1, scanner,))
        #start thread
        thr.start()
