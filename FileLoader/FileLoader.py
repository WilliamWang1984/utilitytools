import glob
import time
import cv2
import numpy as np
import torch
import torchvision

class FileLoader(object):

    def __init__(self, fpath='./', filetype=''):
        self._fpath = fpath
        self._filetype = filetype

        self._lsfiles = glob.glob(self._fpath)

        self._ind_i = 0
        self._ind_max = len(self._lsfiles)


    # decorator function, wrap this around img / txt files    
    def load_next(method):
        def wrapped(*args, **kwargs): 
            
            fl = method(*args, **kwargs)
            #print(type(args[0]))
            #print(isinstance(args[0], ImageLoader), isinstance(args[0], TextLoader))

            #if isinstance(args[0], FileLoader): 

            #    print(fl)            
            #if isinstance(args[0], ImageLoader) or isinstance(args[0], TextLoader):
                
            #    cv2.imshow('', fl)
            #    cv2.waitKey(0)

            return fl

        return wrapped

    @load_next
    def get_img(self, fpath='./', filetype='JPG'):
        return cv2.imread(fpath)

    @load_next
    def get_txt(self, fpath='./', filetype='TXT'):
        return np.loadtxt(fpath, ndmin=2)


class ImageLoader(FileLoader):
    
    #def __init__(self, fpath, filetype):
    #    super(ImageLoader, self).__init__(fpath, filetype)

    @FileLoader.load_next
    def load_next_img(self, fpath=None):
        return cv2.imread(fpath)
        
    def list_imgs(self, gpath='./', img_format='*.jpg'):
        if gpath is not None:
            ls = glob.glob(gpath+'/'+img_format)
            return ls
        elif gpath is None:
            return []


class TextLoader(FileLoader):
    #def __init__(self, fpath, filetype):
    #    super(TextLoader, self).__init__(fpath, filetype)

    @FileLoader.load_next
    def load_next_txt(self, fpath=None):
        return np.loadtxt(fpath, ndmin=2)


class ModelLoader(FileLoader):
    
    def load_weight(self, model_type='ultralytics/yolov5', fpath='./best.pt'):
        return torch.hub.load(model_type, 'custom', fpath)


if __name__ == '__main__':
   
    fl1 = FileLoader()

    ls_imgs = glob.glob('./images/*.jpg')
    ls_txts = glob.glob('./labels/*.txt')

    il1 = ImageLoader()
    tl1 = TextLoader()

    for ls_img in ls_imgs:
        #fl1.get_img(fpath=ls_img, filetype='JPG')
        il1.load_next_img(fpath=ls_img)

    for ls_txt in ls_txts:
        #fl1.get_txt(fpath=ls_txt, filetype='TXT')
        tl1.load_next_txt(fpath=ls_txt)
