#!/bin/bash 
########################################################################################
# Always remember to backup!!! Always remember to backup!!! Always remember to backup!!! 
########################################################################################

# Put this file, together with resize_files.sh to one level above the destination folder(s) 
# E.g. you have folder1, folder2, folder3.... each containing some images for rescale
# Copy and paste batch_resize.sh, resize_files.sh side by side with folder1 to folderN...
# Run batch_resize.sh, this will subsequentially move resize_files.sh traverse through folder1 to folderN,
# in each of these folders, resize_files.sh will attempt to resize every image 

# !!! Note there will be no original files, the originals are replaced with the resized files, so always do a backup before running this script !!!

for i in */
do 
	mv resize_files.sh "$i"
	cd "$i"
	sh resize_files.sh
	mv resize_files.sh ../
	cd ../
done
