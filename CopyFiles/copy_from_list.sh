#!/bin/bash
# courtesy of https://askubuntu.com/questions/596489/how-to-delete-files-listed-in-a-text-file
# Copy all listed files in 'list.txt' to parent level folder
# Alternatively, change 'cp' to 'mv' to move listed files to parent folder instead of copy
# Usage:
# sh copy_from_list.sh list.txt
# 

if [ -z "$1" ]; then
    echo -e "Usage: $(basename $0) FILE\n"
    exit 1
fi

if [ ! -e "$1" ]; then
    echo -e "$1: File doesn't exist.\n"
    exit 1
fi

while read -r line; do
    [ -n "$line" ] && cp -- "$line" ../
done < "$1"
