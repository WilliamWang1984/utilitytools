#!/bin/bash

#    set -e
#    When  this option is on, if a simple command fails for any of the reasons listed in
#              Consequences of Shell Errors or returns an exit status value >0, and is not part of
#              the  compound list following a while, until, or if keyword, and is not a part of an
#              AND or OR list, and is not a pipeline preceded by the !  reserved  word,  then  the
#              shell shall immediately exit.
set -e

#    set -u
#    The shell shall write a message to  standard  error  when  it  tries  to  expand  a
#              variable that is not set and immediately exit. An interactive shell shall not exit.
set -u

#    Change the container name to your container's name
#    Configure in scheduling program (e.g. Cron) for the .sh script to run periodically for auto backup of the container

ALLPS=$(docker ps -f name=nifty_hoover | awk '{ print $1"|"$2 }' | grep -v CONTAINER )
for i in $ALLPS; do
    CTID=$(echo $i | cut -d '|' -f1)
    IMAGE=$(echo $i | cut -d '|' -f2)
    REPO=$(echo $IMAGE | cut -d ':' -f1)
    TAG=$(echo $IMAGE | cut -d ':' -f2)
    DISTROVER=$(echo $TAG | cut -d '-' -f1)
    TIMESTAMP=$(echo $TAG | cut -d '-' -f2)
    TIMESTAMP_NOW=$(date +"%m%d%y_%H%M%S")
    echo "Saving running CTID $CTID to $REPO:$DISTROVER-$TIMESTAMP_NOW"
    docker commit $CTID $REPO:$DISTROVER-$TIMESTAMP_NOW
done
