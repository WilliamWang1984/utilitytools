# Argument comprehension class, accept any number of positional and keyword arguments and organise them into 
# a large dictionary / document of key, value pairs
# the key of positional arguments are defined as 'pos_i', where 'i' is the argument's position index (start from zero)

import argparse
import sys
import os
import re


class ParseTypes(object):
    def __init__(self, args_type=None):
        self.args_type = args_type

    def tuple_type(self, strings):
        strings = strings.replace("(", "").replace(")", "")
        mapped_str = map(str, strings.spilt(","))
        return tuple(mapped_str)


class ParseKwargs(argparse.Action):
    
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, dict())
        for value in values:
            key, value = value.split('=')
            getattr(namespace, self.dest)[key] = value


class Argparser:

    def __init__(self, *pzargs, **kwargs):
        
        # Parse positional (pz) arguments
        pzarguments = {}
        for i, argpz in enumerate(pzargs):
            #print(argpz)
            #print(type(argpz))
            pzarguments['pos_'+str(i)] = argpz

        # Parse keyword (kw) arguments
        #for argkw in kwargs:
            #print(kwargs[argkw])
            #print(type(kwargs[argkw]))
            #print(isinstance(kwargs[argkw], dict))

        print("------------------------------------------------")

        print("self.__dict__ before update: ", self.__dict__)
        
        self.__dict__.update(kwargs)
        print("self.__dict__ after update kwargs: ", self.__dict__)
        
        self.__dict__.update(pzarguments)
        print("self.__dict__ after update pzargs: ", self.__dict__)


    def parse(self, ap, arg_key, arg_val, list_multiple=True):

        # Return the parsed result as a dictionary of {key: value} pair, 
        #     for positional arguments, use position_1, position_2, ...,  position_k as keys
        parsed_args = {}

        #ap = argparse.ArgumentParser()
        # 3 types of passed arguments:
        #     1) Singled posiional argument, singular value of num(int, float), str 
        #     2) Grouped positional argument, array-like, nargs='+' / --parameter_name 'value1' 'value2' 'value3'
        #     3) Grouped keyword argument, array-like, nargs='*', action=ParseKwargs / --parameter_name key1=value1 key2=value2 key3=value3
       
        # Type 1: ordinary singular argument of specific type - isinstance(obj, int / float / str)
        if isinstance(arg_val, int):
            ap.add_argument("--"+arg_key, type=int, default=0)
        if isinstance(arg_val, float):
            ap.add_argument("--"+arg_key, type=float, default=0.0)
        if isinstance(arg_val, str):
            ap.add_argument("--"+arg_key, type=str, default="")
        
        # Type 2: array of positional arguments - isinstance(obj, list)
        if isinstance(arg_val, list):
            
            if list_multiple==True:
                ap.add_argument("--"+arg_key, nargs='+', action='append')
            else:
                ap.add_argument("--"+arg_key, nargs='+')
 

        # Type 3: array of keywords arguments - isinstance(obj, dict)
        if isinstance(arg_val, dict):
            ap.add_argument("--"+arg_key, nargs='*', action=ParseKwargs)

        #return ap


    # Courtesy of 'kert':
    # https://stackoverflow.com/questions/17075071/is-there-a-python-method-to-access-all-non-private-and-non-builtin-attributes-of
    def list_public_dict(self):
        return [(k, v) for k, v in vars(self).items() if
                not (k.startswith('_') or callable(v))]



    def compre_sys_argv(self, ls_sys_argv):
        #print(ls_sys_argv)
        ls_args_as_tuple = []
        for i, sys_argv_i in enumerate(ls_sys_argv):
            if '=' in sys_argv_i:
                # sys_argv_i this is a key=val pair (kwarg)
                tup_i = sys_argv_i.split('=')
                key_i = tup_i[0] # left of '='
                val_i = tup_i[1] # right of '='

            else:
                # sys_argv_i is a val (pzarg)
                key_i = 'position_'+str(i)
                val_i = sys_argv_i

            ls_args_as_tuple.append((key_i, val_i))

        return ls_args_as_tuple


    def lstuple_to_dict(self, lstup1):
        dic1 = {}
        for k, v in lstup1:
            dic1.setdefault(k, v)
        return dic1


if __name__ == '__main__':
    

    pzvalue1 = 7898
    pzvalue2 = 'asdfsdtew'
    pzvalue3 = ['a', 'b', 'c', 'd', 'e', 'f']
    pzvalue4 = {'k1': 'v1', 'k2': 'v2', 'k3': 'v3', 'k4': 'v4'}
    #ap1 = Argparser(pzvalue1, pzvalue2, pzvalue3, pzvalue4, key1='value1', key2=2, key3=1.223423, key4=['str1', 234234, 2342.1232, 'embed_key'], key5={'a1':'aa1', 'b1':'bb1', 'c1':'cc1'})

    ap1 = Argparser()

    #for ap_i in dir(ap1):
    #    print(ap_i)

    print(ap1.list_public_dict())

    ap = argparse.ArgumentParser()

    for item in ap1.list_public_dict():
        ap1.parse(ap, item[0], item[1])

    print(vars(ap.parse_args()))
