from Argparser import Argparser
import sys

if __name__ == '__main__':
    v1 = 3.1415926789
    t = {'k1': v1}
    ap1 = Argparser(5, 5.907, 'whatever_str', [0, 'a', 9.8, t], {'p_dict_key': 'p_dict_val'}, key1='this is my value1', key2='my foo', key3=['we', 'should', 'open', 'a', 'bar'], key4={'rand_key': 'asr3j84390tjsfdjng3465fg', 'rigid_key': 'AAAAAAAAAAA', 'embedded_key5': [{'I_am':0},{'more': 1},{'complikes': 2}]})

    print("Printing a list of instance initialized arguments (as tuples)")
    print(ap1.list_public_dict())

    #print(len(sys.argv))
    #for i in sys.argv[1:]:
    #    print(i)

    sys_argv_dict = ap1.compre_sys_argv(sys.argv[1:])

    print("Printing a list of sys.argv arguments (as tuples)")
    print(sys_argv_dict)
