#!bin/python

# Note how the dictionary objects are quoted to prevent them recognised as single key => multiple values mapping
python3 test_argparse.py 0 2.234 's3sfas' ['5',8,9.1,37] '{'a':'aa1','b':'bb','c':'cc7'}' key1='myKey1' key2=['sacred','ji','is','with','me'] key3=['{'this':0,'is':1,'a':2,'bit':3,'tea':4}']

