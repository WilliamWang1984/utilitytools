# Author: William WANG
# Organization: Griffith IIIS

# Courtesy of:
#https://medium.com/pythonhive/python-decorator-to-measure-the-execution-time-of-methods-fa04cb6bb36d
#https://medium.com/survata-engineering-blog/monitoring-memory-usage-of-a-running-python-program-49f027e3d1ba

# Profiler Class used to measure the time and memory (ram) cost of python functions by using
# decorator wrapper functions:
# @timeit and @ramit
# To use, define profiler = Profiler(), optionally provide log_name and formatting strings for timeit and ramit 
# 
# @profiler.timeit
# def your_function_to_measure(...):
#     ...
# 
# if __name__ == __main()__:
#        your_function_to_measure(...)

import time
import tracemalloc
import cmath

class Profiler:
    def __init__(self, log_name='log_name', time_format='%2.2f', ram_format='%4.3f'):
        self.log_name = log_name
        self.time_format = time_format
        self.ram_format = ram_format


    def timeit(self, method):
        def timed(*args, **kw):
            ts = time.time()
            result = method(*args, **kw)
            te = time.time()        
            if self.log_name in kw:
                # keep timed data in dictionary as parameter
                name = kw.get('log_time', method.__name__.upper())
                kw[self.log_name][name] = int((te - ts) * 1000)
            else:
                # else simple print timed data on screen
                formatting_string = '%r  '+self.time_format+' ms'
                print(formatting_string % (method.__name__, (te - ts) * 1000))
            return result    
        return timed


    def ramit(self, method):
        def ramd(*args, **kw):
            tracemalloc.start()
            result = method(*args, **kw)
            current, peak = tracemalloc.get_traced_memory()
            if self.log_name in kw:
                name = kw.get('log_ram', method.__name__.upper())
                kw[self.log_name][name] = [current / 10**6, peak / 10**6]
            else:
                formatting_string = '%r [current '+self.ram_format+' MB, peak '+self.ram_format+' MB]'
                print(formatting_string % (method.__name__, current/10**6, peak/10**6))
            return result
        return ramd

