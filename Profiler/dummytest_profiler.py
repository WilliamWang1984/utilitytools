from Profiler import Profiler

prof = Profiler()

#@prof.timeit
@prof.ramit
def dummy_test_function(test, **kwargs):
    # print positional argument
    print(test)
    # print inheriant conent
    print('dummy_function for content test')

logs = {}
custom_time_format = '%2.1f'
custom_ram_format = '%2.4f'
dummy_test_function('test me for anyting!!!', log_name=logs, time_format=custom_time_format, ram_format=custom_ram_format)

print(logs)

