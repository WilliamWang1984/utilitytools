from LabelConverter import LabelConverter
import numpy as np

if __name__ == '__main__':

    lc1 = LabelConverter()
    test_arr = [[100, 120, 200, 220, 0.7, 0], 
                [210, 280, 30, 130, 0.8, 1],
                [330, 378, 57, 129, 0.9, 1],
                [112, 223, 440, 500, 0.8, 0],
                [450, 600, 225, 300, 0.7, 1]]

    test_arr_np = np.array(test_arr)

    #swapped = lc1.swap_columns(np.array(test_arr_np))

    #print(swapped)


    print(test_arr)

    selected = lc1.select_rows(np.array(test_arr_np), col_index=5, ls_test_col_values=[0, 1, 2])

    print(selected)
