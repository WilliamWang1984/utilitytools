import numpy as np
import os, sys
from scipy import spatial
import argparse
from glob import glob
from multipledispatch import dispatch

if not '/home/william/utilitytools' in sys.path:
    sys.path.append('/home/william/utilitytools')

from Argparser.Argparser import ParseKwargs
from Argparser.Argparser import ParseTypes
pt1 = ParseTypes()


#def tuple_type(strings):
#    strings = strings.replace("(", "").replace(")", "")
#    mapped_str = map(str, strings.spilt(","))
#    return tuple(mapped_str)


# class ROI: Region of Interest
class ROI(object):

    def __init__(self, roi_x0=0, roi_x1=1080, roi_y0=0, roi_y1=1080):
        self.roi_x0 = roi_x0
        self.roi_x1 = roi_x1
        self.roi_y0 = roi_y0
        self.roi_y1 = roi_y1


    # Get intersection over union for a pair of bounding boxes
    def get_iou(self, bb1, bb2):
        """
        Calculate the Intersection over Union (IoU) of two bounding boxes.

        Parameters
        ----------
        bb1 : list [x1, x2, y1, y2, conf_score, cls_index]
            The (x1, y1) position is at the top left corner,
            the (x2, y2) position is at the bottom right corner
        bb2 : list [x1, x2, y1, y2, conf_score, cls_index]
            The (x1, y1) position is at the top left corner,
            the (x2, y2) position is at the bottom right corner

        Returns
        -------
        float
            in [0, 1]
        """
        assert bb1[0] < bb1[2]
        assert bb1[1] < bb1[3]
        assert bb2[0] < bb2[2]
        assert bb2[1] < bb2[3]

        # determine the coordinates of the intersection rectangle
        x_left = max(bb1[0], bb2[0])
        y_top = max(bb1[1], bb2[1])
        x_right = min(bb1[2], bb2[2])
        y_bottom = min(bb1[3], bb2[3])

        if x_right < x_left or y_bottom < y_top:
            return 0.0

        # The intersection of two axis-aligned bounding boxes is always an
        # axis-aligned bounding box
        intersection_area = (x_right - x_left) * (y_bottom - y_top)

        # compute the area of both AABBs
        bb1_area = (bb1[2] - bb1[0]) * (bb1[3] - bb1[1])
        bb2_area = (bb2[2] - bb2[0]) * (bb2[3] - bb2[1])

        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
        assert iou >= 0.0
        assert iou <= 1.0
        return iou


    # Get nearest bounding boxes
    def find_nearest_bounding_boxes(self, query_box, preds_coordinates, preds_array, n=1):
        matched_rows = spatial.KDTree(preds_coordinates).query(query_box)[n]
        qres = preds_array[matched_rows]

        return qres


class LabelConverter(object):

    def __init__(self, x=0, y=0, w=0, h=0):
        self.x = x
        self.y = y
        self.w = w
        self.h = h


    # scenarios of conversions:
    #   1. single-class labels to multi-class 
    #       a. folders: class_0 {0.txt, 1.txt, ..., n.txt}, class_1 {0.txt, 1.txt,... }, class_2 {...}, class_3 {...}, class_4 {...}
    #       b. one-folder: merged {0.txt, 1.txt, ..., n.txt}
    #   2. multi-class labels to single-class
    #       a. one-folder {0.txt, 1.txt, ..., n.txt}
    #       b. splited folders class_0, class_1, ..., class_n
    
    #   3. update same-boxes different classes to singe-box single combinatorial class
    #       a. mapping {class_0, class_1, ..., class_n} to {class_0, class_1, class_2, ..., class_2^n} 
    
    #   4. remove classes
    #       a. {class_0, class_1, ..., class_n} ==> {class_0, class_1, ..., class_(n-k)}
    #   5. update classes
    #       a. {class_(n-k), class_(n-k+1), ..., class_n} ==> {class_0, class_1, class_(k-1)}


    def remove_class_ids(self, np_arr, low_t=0, high_t=7, keep_id_offset=False, class_idx=0):
        #print(np_arr)
        lowpass = np_arr[np_arr[:, class_idx] <= high_t, :]
        highpass = np_arr[np_arr[:, class_idx] >= low_t, :]
        
        #column0_high = highpass[:, class_idx]    
        #minclassid_high = min(column0_high) if len(column0_high) > 0 else 0

        bandpass = lowpass[lowpass[:, class_idx] >= low_t, :]
        #column0_band = bandpass[:, class_idx]
        #minclassid_band = min(column0_band) if len(column0_band) > 0 else 0

        if not keep_id_offset:
            highpass[:, class_idx] -= low_t #minclassid_high
            bandpass[:, class_idx] -= low_t #minclassid_band
        #print(highpass)
        #print(low_t)
        return lowpass, highpass, bandpass


    def reassign_class_ids(self, np_arr, mapping_dict):
        reassigned = []
        mapped_idx = -1
        for row in np_arr:
            #print(int(row[0]))
            #print(mapping_dict)
            mapped = mapping_dict.get(str(int(row[0])))
            if mapped:
                mapped_idx = mapping_dict[str(int(row[0]))]
            reassigned.append([int(mapped_idx), row[1], row[2], row[3], row[4]])

        return np.array(reassigned)


    def reinvent_class_ids(self, np_arr, grps_classes_ids):
        # grps_classes_ids = [[0, 1], [3, 4, 5], [2, 6]]

        expanded_arr = []

        n_grps = len(grps_classes_ids)

        #for j, g in enumerate(grps_classes_ids):
            # e.g.
            # g = [0, 1]
            #     [3, 4, 5]
            #     [2, 6]
         
        # class ids of the first groupp
        g0 = grps_classes_ids[0]
        
        # class ids of the remaining groups
        g1p = np.concatenate(grps_classes_ids[1:])

        np_select_0 = []
        for i, h in enumerate(g):
            # for every class in the 1st group, find rows of that class
            np_slice = np_arr[np_arr[:, 0] == h, :]

            # set group np first row
            if i == 0:
                np_select_0 = np_slice
            else:
                # add rows to group np
                np_select_0 = np.stack((np_select_0, np_slice))
        
        np_select_g1p = []
        for i, h1 in enumerate(g1p):
            np_slice = np_arr[np_arr[:,0] == h1, :]

            if i == 0:
                np_select_g1p = np_slice
            else:
                np_select_g1p = np.stack((np_select_g1p, np_slice))

        # np_select_0: all rows of group 0 class ids

        # np_select_g1p: all rows of other group class ids
        
        expanded_arr = []
        new_id = 0
        for i, bb in enumerate(np_select_0):
            coors_np_g1p = np.delete(np_select_g1p, 0, 1)
            new_id += 2 ** bb[0]
            coord_bb = [bb[1], bb[2], bb[3], bb[4]]
            # get the n-1 matching bounding boxes for all other class groups
            matching_rows = np_select_g1p[spatial.KDTree(coors_np_g1p).query(coord_bb)[n_grps-1]]
            
            # for all other class ids from other groups matching the same bb,
            # add their class weighing coefficients to the new_id
            for row in matching_rows:
                new_id += 2 ** row[0]

            expanded_arr.append([new_id, bb[1], bb[2], bb[3], bb[4]])

        # new_id vs previous un-expanded id

        # Old ids:
        # class_0: 0   # class group 1
        # class_1: 1
        # class_2: 2
        # ...
        # ----------------------------------------
        # class_k: k         # class group 2 (10 < k < 100, e.g. k == 11)
        # class_(k+1): k+1
        # ...
        # ----------------------------------------
        # class_n: n      # class group 3   (n >= 100, e.g. n == 100)
        # class_(n+1): n+1 


        # New ids:
        # class_0_AND_class_(k+5)_AND_class_(n+1):     2^0 + 2^16 + 2^101
        # class_1_AND_class_(k+2)_AND_class_n:         2^1 + 2^13 + 2^100
        # class_5_AND_class_(k+50)_AND_class_(n+200):  2*5 + 2^61 + 2^300

        return expanded_arr


    def split_annotation_by_class_id(self, np_arr, n_cls):
        splitted = []
        for i in range(0, n_cls):
            np_slice = np_arr[np_arr[:, 0] == i, :]
            if not np_slice.size == 0:
                splitted.append(np_slice)

        return splitted


    def remove_classid_subdir(self, txt):
        ls_segs = txt.split('/')
        prefix = '/'.join(ls_segs[:-2])
        return prefix+'/'+ls_segs[-1], ls_segs[-2], ls_segs[-1]


    def merge_annotations_by_filename(self, ls_subclass_txts, n_cls):
        np_arr = []
        list_name = []

        for i, txtname in enumerate(ls_subclass_txts):
            
            tf = open(txtname, 'r')

            # if file is not empty
            if os.fstat(tf.fileno()).st_size:
                np_txt = np.loadtxt(txtname, ndmin=2)
                # np_txt = [[0 0.128 0.437 0.057 0.132] 
                #           [0 0.367 0.781 0.143 0.077]
                #           [0 0.222 0.543 0.714 0.091]]

            tf.close()


            merged_txt_path, cls_id_str, current_filename = self.remove_classid_subdir(txtname)
            # ./home/user_name/images/dir/img1.txt, img1.txt
            if merged_txt_path in list_name:
                # merged classes txt file already exists, append current np to 
                # existing np (rows of all first and previous nps that extracted from the txt with current name)
                idx = list_name.index(merged_txt_path)
                
                #print("array A: ", np_arr[idx])
                #print("array B: ", np_txt)

                np_txt[:, 0] = int(cls_id_str)
                np_arr[idx] = np.vstack((np_arr[idx], np_txt))
            else:
                # create merged classes np, append the first np to the merged classes np 
                list_name.append(merged_txt_path)
                np_txt[:, 0] = int(cls_id_str)
                np_arr.append(np_txt)

        return list_name, np_arr


    # [x] / coordinates, (img_h, img_w), {<definition of params>}
    @dispatch(list, tuple, dict)
    def xyxy2xywh(self, x, img_size=(1080, 1440), pdict={'xmin':0,'ymin':1,'xmax':2,'ymax':3,'conf':-1,'cind':-1,'im_width':1440,'im_height':1080}):
        '''
        Converts darknet ouput file - e.g. (x1, y1, x2, y2, class, conf.)
        to yolo trainable labels - e.g. (class, x, y, w, h)
        '''
        #print(pdict)
        #print(x)
        
        xmin = x[int(pdict['xmin'])]
        xmax = x[int(pdict['xmax'])]
        ymin = x[int(pdict['ymin'])]
        ymax = x[int(pdict['ymax'])]
        #classIndex = x[int(pdict['cind'])]
        #print(img_size[0])

        xcen = float(((xmin + xmax)/2.0) / int(pdict['im_width']))
        ycen = float(((ymin + ymax)/2.0) / int(pdict['im_height']))

        w = float((xmax - xmin) / int(pdict['im_width']))
        h = float((ymax - ymin) / int(pdict['im_height']))

        return xcen, ycen, w, h


    # xc, yc, w, h, width, height
    @dispatch(float, float, float, float, int, int)
    def xywh2xyxy(xc, yc, w, h, img_width=1224, img_height=1224):
        x0 = xc - w/2.0
        x1 = xc + w/2.0
        y0 = yc - h/2.0
        y1 = yc + h/2.0

        return int(x0 * img_width), int(y0 * img_height), int(x1 * img_width), int(y1 * img_height)


    @dispatch(list, tuple, dict)
    def xywh2xyxy(self, x, img_size=(1080, 1440), pdict={'xcenter':1,'ycenter':2,'width':3,'height':4,'conf':-1,'cind':0,'im_width':1440,'im_height':1080}):
        '''
        Converts yolo training label - e.g. (x, y, w, h)
        to openCV (Darknet) format - e.g. (x1, y1, x2, y2)
        img_size = (im_heigh, im_width)
        '''
        im_width = int(img_size[1]) if img_size is not None else int(pdict['im_width'])
        im_height = int(img_size[0]) if img_size is not None else int(pdict['im_height']) 

        xcen = x[int(pdict['xcenter'])] * int(im_width)
        ycen = x[int(pdict['ycenter'])] * int(im_height)
        w    = x[int(pdict['width'])]   * int(im_width)
        h    = x[int(pdict['height'])]  * int(im_height)
        
        w = 1 if w < 1 else w
        h = 1 if h < 1 else h
        
        x0 = int(xcen - (w/2))
        y0 = int(ycen - (h/2))
        x1 = int(x0 + w)
        y1 = int(y0 + h)

        return x0, y0, x1, y1


    def xyxys2xywhs(self, arr, pdict={'cind':0, 'xmin': 1, 'ymin': 2, 'xmax': 3, 'ymax': 4, 'im_width': 1224, 'im_height': 1224}):
    	ls_xywh = []
    	for bb in arr:
    	    xc, yc, w, h = self.xyxy2xywh(bb, (pdict['im_height'], pdict['im_width']), pdict)
    	    ls_xywh.append([bb[pdict['cind']], xc, yc, w, h])
    	
    	return np.array(ls_xywh)


    def xywhs2xyxys(self, arr, pdict={'cind':0, 'xcenter':1, 'ycenter':2, 'width':3, 'height':4, 'im_width':1224, 'im_height':1224}):
        ls_xyxy = []
        for bb in arr:
            x0, y0, x1, y1 = self.xywh2xyxy(float(bb[pdict['xcenter']]), float(bb[pdict['ycenter']]), float(bb[pdict['width']]), float(bb[pdict['height']]), int(pdict['im_width']), int(pdict['im_height']))
            ls_xyxy.append([bb[pdict['cind']], x0, y0, x1, y1])

        return np.array(ls_xyxy)


    def translate_coordinates(self, np_arr, origin_width=640, origin_height=640, padded_width=1440, padded_height=1080):
        #if offset_x > 1 or offset_y > 1:
        #    # xyxy coordinate system
        #elif offset_x < 1 and offset_y < 1:
        #    # xywh coordinate system

        #            id    x_c    y_c     w      h
        # np_arr = [[0   0.2321 0.1587 0.3322 0.4768], 
        #           [1   0.3419 0.7722 0.4441 0.2516],
        #            ...
        #           [6   0.3331 0.7797 0.2252 0.7861]]

        n_boxes = len(np_arr)

        offset_x = int((padded_width - origin_width) / 2.0)
        offset_y = int((padded_height - origin_height) / 2.0)
        #print(offset_x, offset_y)
        ls_npr = []
        for np_r in np_arr:
            x0, y0, x1, y1 = self.xywh2xyxy(np_r, img_size=[origin_height, origin_width])
            #print(int_npr)
            xc, yc, w, h = self.xyxy2xywh([x0+offset_x, y0+offset_y, x1+offset_x, y1+offset_y])
            ls_npr.append([np_r[0], xc, yc, w, h])
        
        #print(ls_npr)
        return np.array(ls_npr)


    # Column swapping to save yolo predictions to ground-truth compatible labels, 
    # using swap sequence [[4, 5], [0, 4], [1, 5], [2, 4], [3, 5]]
    # Example for column swappping
    # [[x0 x1 y0 y1 conf cls]               [[x0 x1 y0 y1 cls conf]    
    #  [x0 x1 y0 y1 conf cls]    [4, 5]      [x0 x1 y0 y1 cls conf] 
    #      ...                   =====>        ...
    #  [x0 x1 y0 y1 conf cls]]               [x0 x1 y0 y1 cls conf]]
    # --------------------------------------------------------------
    # [[x0 x1 y0 y1 cls conf]               [[cls x1 y0 y1 x0 conf]    
    #  [x0 x1 y0 y1 cls conf]    [0, 4]      [cls x1 y0 y1 x0 conf] 
    #      ...                   =====>        ...
    #  [x0 x1 y0 y1 cls conf]]               [cls x1 y0 y1 x0 conf]]
    # --------------------------------------------------------------
    # [[cls x1 y0 y1 x0 conf]               [[cls conf y0 y1 x0 x1]    
    #  [cls x1 y0 y1 x0 conf]    [1, 5]      [cls conf y0 y1 x0 x1] 
    #      ...                   =====>        ...
    #  [cls x1 y0 y1 x0 conf]]               [cls conf y0 y1 x0 x1]]
    # --------------------------------------------------------------
    # [[cls conf y0 y1 x0 x1]               [[cls conf x0 y1 y0 x1]    
    #  [cls conf y0 y1 x0 x1]    [2, 4]      [cls conf x0 y1 y0 x1] 
    #      ...                   =====>        ...
    #  [cls conf y0 y1 x0 x1]]               [cls conf x0 y1 y0 x1]]
    # --------------------------------------------------------------
    # [[cls conf x0 y1 y0 x1]               [[cls conf x0 x1 y0 y1]    
    #  [cls conf x0 y1 y0 x1]    [3, 5]      [cls conf x0 x1 y0 y1] 
    #      ...                   =====>        ...
    #  [cls conf x0 y1 y0 x1]]               [cls conf x0 x1 y0 y1]]
    # --------------------------------------------------------------
    def swap_columns(self, arr, swap_cols=[[4,5],[0,4],[1,5],[2,4],[3,5]]):
        #print("before swap", arr)
        
        for swap_col in swap_cols:

            col = swap_col.copy()
            swap_col.reverse()
            arr[:, col] = arr[:, swap_col]
        
        #arr[:, [4,5]] = arr[:, [5,4]]
        #arr[:, [0,4]] = arr[:, [4,0]]
        #arr[:, [1,5]] = arr[:, [5,1]]
        #arr[:, [2,4]] = arr[:, [4,2]]
        #arr[:, [3,5]] = arr[:, [5,3]]
              
        #print("after swap", arr)
        return arr


    # Employing the np.in1d() test facility to select np rows that satisfy np column_index IN ls_test_col_values
    def select_rows(self, arr, col_index=0, ls_test_col_values=[0]):
        #print(arr)
        #print(col_index)
        #print(np.array(ls_test_col_values))

        #print(np.in1d(arr[:, col_index], np.array([0])))
        arr_select = arr[np.in1d(np.array(arr[:, col_index]), ls_test_col_values)]
        return arr_select


    # Just a simple string text replace function should be a lambda
    def find_alternative_txt_path(self, origin_txt_path, origin_path_kwd='images', alternative_txt_path='frames'):
        return origin_txt_path.replace(origin_path_kwd, alternative_txt_path)

    # Just a simple np array merging function that should be a lambda
    def merge_alternative_txt(self, np_origin, np_append):
        return np.vstack((np_origin, np_append))


if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--path", type=str, default="./images/*.txt", help="Path to txt label files")
    ap.add_argument("-q", "--dir", type=str, default="./images/dirs/*", help="Directory to label class subfolders")
    ap.add_argument("-l", "--lower_thres", type=int, default=0, help="Labels downto class id to keep, remove all classes whose id smaller than t")
    ap.add_argument("-u", "--upper_thres", type=int, default=8, help="Labels upto class id t to keep, remove all classes whose id greater than t")
    ap.add_argument("-m", "--mapping", nargs="*", action=ParseKwargs, help="Mappings from original class ids to new class ids (e.g. -m 4=0 5=1 6=2 7=3)")
    ap.add_argument("-x", "--update_mode", type=str, default="delete", help="Update mode for the yolo labels (delete / update / expand / singulate / pluralize / offset)")
    ap.add_argument("-f", "--format", type=pt1.tuple_type, default=('%d', '%.4f', '%.4f', '%.4f', '%.4f'), help="annotation line format (e.g. 1<int> 0.2341<float> 0.4878<float> 0.0925<float> 0.3178<float>)")
    ap.add_argument("-g", "--groups", type=int, nargs='+', action='append', default=[[0, 1], [4, 5, 6, 7]], help="Groups of class_ids (e.g. -g 0 1 -g 4 5 7 -g 2 3 6)")
    ap.add_argument("-n", "--n_classes", type=int, default=8, help="Number of classes in the annotation files")
    
    ap.add_argument("-c", "--origin_width", type=int, default=640, help="Original coordinate image width")
    ap.add_argument("-r", "--origin_height", type=int, default=640, help="Original coordinate image height")

    ap.add_argument("-d", "--padded_width", type=int, default=1440, help="Padded coordinate image width")
    ap.add_argument("-s", "--padded_height", type=int, default=1080, help="Padded coordinate image height")
    ap.add_argument("-a", "--alternative_txt_path", type=str, default="frames", help="Path to additional txt annotations to merge with")

    args = vars(ap.parse_args())
    
    '''
    l, h, b = remove_class_ids(np.array([[0, 220, 330, 250, 380],
                                [1, 450, 210, 480, 260],
                                [2, 300, 500, 350, 600],
                                [3, 100, 200, 150, 300],
                                [4, 250, 340, 330, 440],
                                [5, 80, 900, 380, 1250],
                                [6, 70, 100, 130, 220],
                                [7, 110, 530, 200, 720]]), low_t=2, high_t=6)
    #print(l)
    #print(h) 
    #print(b)
    
    r = reassign_class_ids(np.array([[0, 220, 330, 250, 380],
                                [1, 450, 210, 480, 260],
                                [2, 300, 500, 350, 600],
                                [3, 100, 200, 150, 300],
                                [4, 250, 340, 330, 440],
                                [5, 80, 900, 380, 1250],
                                [6, 70, 100, 130, 220],
                                [7, 110, 530, 200, 720]]), args["mapping"])
    print(r)

    '''

   
    sub_txts = []
    ls_dirs = glob(args["dir"])
    for sub in ls_dirs:
        ls_sub_txts = glob(sub+'/*.txt')
        for sub_txt in ls_sub_txts:
            #print(sub_txt)
            sub_txts.append(sub_txt)

    #print(args["mapping"])
    #fmt1 = '%d', '%.4f', '%.4f', '%.4f', '%.4f'

    lc1 = LabelConverter()

    ls_txts = glob(args["path"])
 
    for txt in ls_txts:
        tf = open(txt, 'r')
        if os.fstat(tf.fileno()).st_size:
            # if file size is not zero
            np_txt = np.loadtxt(txt, ndmin=2)

            txt_paths1 = txt.split('/')
            txt_paths = '/'.join(txt_paths1[0:-1])

            if args["update_mode"] == 'delete':
                np_low, np_high, np_band = lc1.remove_class_ids(np_txt, low_t=args["lower_thres"], high_t=args["upper_thres"])                
                np.savetxt(txt, np_band, delimiter=' ', fmt=args["format"])

            elif args["update_mode"] == 'update':
                np_reassign  = lc1.reassign_class_ids(np_txt, args["mapping"])
                np.savetxt(txt, np_reassign, delimiter=' ', fmt=args["format"])

            elif args["update_mode"] == 'expand':
                np_reinvent = lc1.reinvent_class_ids(np_txt, args["groups"])
                np.savetxt(txt, np_reinvent, delimiter=' ', fmt=args["format"])

            elif args["update_mode"] == 'singulate':
                # split one annotation .txt of n classes into n .txt files of single class each, keep name unchanged, make different folders for each class
                np_by_classes = lc1.split_annotation_by_class_id(np_txt, args["n_classes"])
                #print(np_by_classes)
                #print(txt)
                for np_class in np_by_classes:
                    #print(np_class)
                    class_id = np_class[0][0]
                    #print(txt_paths)
                    new_txt_path = txt_paths+'/'+str(int(class_id))
                    #print(new_txt_path)
                    if not os.path.exists(new_txt_path):
                        # create such path
                        os.mkdir(new_txt_path, mode = 0o755)
                    np_class[:, 0] = 0
                    np.savetxt(new_txt_path+'/'+txt_paths1[-1], np_class, delimiter=' ', fmt=args["format"])

            elif args["update_mode"] == 'pluralize':
                # assemble n .txt single class annotation files of the same name, in different folders to one .txt annotation file of multiple class, keep name unchanged.
                
                ls_names, ls_np_by_txts = lc1.merge_annotations_by_filename(sub_txts, args["n_classes"])

                for name_i, np_i in zip(ls_names, ls_np_by_txts):
                    np.savetxt(name_i, np_i, delimiter=' ', fmt=args["format"])

            elif args["update_mode"] == 'offset':

                np_trans = lc1.translate_coordinates(np_txt, origin_width=args["origin_width"], origin_height=args["origin_height"], padded_width=args["padded_width"], padded_height=args["padded_height"])
                np.savetxt(txt, np_trans, delimiter=' ', fmt=args["format"])

            elif args["update_mode"] == 'merge':
                print("origin path: ", txt)
                to_append_txt_path = lc1.find_alternative_txt_path(txt)
                print("addition_path:", to_append_txt_path)
                
                to_append_txt = np.loadtxt(to_append_txt_path, ndmin=2)

                np_merged = lc1.merge_alternative_txt(np_txt, to_append_txt)
                #print(len(np_txt))
                #print(len(np_merged))

                #print(txt)

                np.savetxt(txt, np_merged, delimiter=' ', fmt=args["format"])
                print(txt, ' saved.')

        else:
            print("file: "+ txt +" is empty")

        tf.close()


