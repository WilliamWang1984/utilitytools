#!/bin/bash 
# Courtesy of https://askubuntu.com/questions/935498/decomposing-image-red-channel
########################################################################################
# Always remember to backup!!! Always remember to backup!!! Always remember to backup!!! 
########################################################################################

# Put this file into the destination folder containing multiple images
# Adjust the image extension "*.jpg", channel parameter (< -channel R > is for red channel) 
# You may change the second "{}" expression (i.e. "{}" at the end of the line) to save the decomposed channel into a different name. 
# You may also change the max traverse depth {-maxdepth N (N being a natural number)} to process subfolders and subfolders of subfolders etc. 
# This resize_files.sh will attempt to decompose every image and save the decomposed images to the save name under subfolders {RedChannel, GreenChannel, BlueChannel}
# !!! To prevent loss from unexpected IO errors, always remember to do a backup before running !!!

mkdir RedChannel/
cp *.jpg RedChannel
cp decompose_files.sh RedChannel
cd RedChannel
find . -maxdepth 1 -iname "*.jpg" | xargs -L1 -I{} convert "{}" -channel R -separate "{}"
rm decompose_files.sh
cd ../

mkdir GreenChannel/
cp *.jpg GreenChannel
cp decompose_files.sh GreenChannel
cd GreenChannel
find . -maxdepth 1 -iname "*.jpg" | xargs -L1 -I{} convert "{}" -channel G -separate "{}"
rm decompose_files.sh
cd ../

mkdir BlueChannel/
cp *.jpg BlueChannel
cp decompose_files.sh BlueChannel
cd BlueChannel
find . -maxdepth 1 -iname "*.jpg" | xargs -L1 -I{} convert "{}" -channel B -separate "{}"
rm decompose_files.sh
cd ../

