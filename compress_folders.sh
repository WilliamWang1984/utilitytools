#!/bin/bash
# Courtesy of https://stackoverflow.com/questions/20452384/how-to-compress-multiple-folders-each-into-its-own-zip-archive
# Copy this script side by side with folder(s) you wish to compress into zip(s)
# Run 'sh compress_folders.sh'
# Watch the terminal output realtime info. on files being zipped. and see <FolderName>.zip being generated in the same folder 
for i in */; do zip -r "${i%/}.zip" "$i"; done
