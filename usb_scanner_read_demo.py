import usb.core as uc
import usb.util as uu
import cv2
import argparse
from multiprocessing import Pool
import threading

"""
This uses the pyusb module to read the popular LS2208 USB barcode 
scanner in Linux. This barcode scanner is sold under many labels. It
is a USB HID device and shows up as a keyboard so a scan will "type"
the data into any program. But that also becomes its limitation. Often,
you don't want it to act like it's typing on a keyboard; you just want
to get data directly from it.

This program detaches the device from the kernel so it will not "type"
anything, and reads directly from the device without needing any device-
specific USB drivers.

It does use pyusb to do low-level USB comm. See 
https://github.com/pyusb/pyusb for installation requirements. Basically, 
you need to install pyusb:

    pip install pyusb

To install the backend that pyusb uses, you can use libusb (among
others):

    sudo apt install libusb-1.0-0-dev
    
You need to make sure your user is a member of plugdev to use USB 
devices (in Debian linux):

    sudo addgroup <myuser> plugdev
    
You also need to create a udev rule to allow all users to access the
barcode scanner. In /etc/udev/rules.d, create a file that ends in .rules
such as 55-barcode-scanner.rules with these contents:

    (TBD)
"""

# VendorID: 05e0
# ProductID: 1200

# ProductID: 1701 - CDC: Communication Device Class
def cdc2ascii(lst):
    line = ''
    for i in lst:
        line += chr(i)
    return line
    
def hid2ascii(lst):
    """The USB HID device sends an 8-byte code for every character. This
    routine converts the HID code to an ASCII character.
    
    See https://www.usb.org/sites/default/files/documents/hut1_12v2.pdf
    for a complete code table. Only relevant codes are used here."""
    
    # Example input from scanner representing the string "http:":
    #   array('B', [0, 0, 11, 0, 0, 0, 0, 0])   # h
    #   array('B', [0, 0, 23, 0, 0, 0, 0, 0])   # t
    #   array('B', [0, 0, 0, 0, 0, 0, 0, 0])    # nothing, ignore
    #   array('B', [0, 0, 23, 0, 0, 0, 0, 0])   # t
    #   array('B', [0, 0, 19, 0, 0, 0, 0, 0])   # p
    #   array('B', [2, 0, 51, 0, 0, 0, 0, 0])   # :
    
    assert len(lst) == 8, 'Invalid data length (needs 8 bytes)'
    conv_table = {
        0:['', ''],
        4:['a', 'A'],
        5:['b', 'B'],
        6:['c', 'C'],
        7:['d', 'D'],
        8:['e', 'E'],
        9:['f', 'F'],
        10:['g', 'G'],
        11:['h', 'H'],
        12:['i', 'I'],
        13:['j', 'J'],
        14:['k', 'K'],
        15:['l', 'L'],
        16:['m', 'M'],
        17:['n', 'N'],
        18:['o', 'O'],
        19:['p', 'P'],
        20:['q', 'Q'],
        21:['r', 'R'],
        22:['s', 'S'],
        23:['t', 'T'],
        24:['u', 'U'],
        25:['v', 'V'],
        26:['w', 'W'],
        27:['x', 'X'],
        28:['y', 'Y'],
        29:['z', 'Z'],
        30:['1', '!'],
        31:['2', '@'],
        32:['3', '#'],
        33:['4', '$'],
        34:['5', '%'],
        35:['6', '^'],
        36:['7' ,'&'],
        37:['8', '*'],
        38:['9', '('],
        39:['0', ')'],
        40:['\n', '\n'],
        41:['\x1b', '\x1b'],
        42:['\b', '\b'],
        43:['\t', '\t'],
        44:[' ', ' '],
        45:['_', '_'],
        46:['=', '+'],
        47:['[', '{'],
        48:[']', '}'],
        49:['\\', '|'],
        50:['#', '~'],
        51:[';', ':'],
        52:["'", '"'],
        53:['`', '~'],
        54:[',', '<'],
        55:['.', '>'],
        56:['/', '?'],
        100:['\\', '|'],
        103:['=', '='],
        }

    # A 2 in first byte seems to indicate to shift the key. For example
    # a code for ';' but with 2 in first byte really means ':'.
    if lst[0] == 2:
        shift = 1
    else:
        shift = 0
        
    # The character to convert is in the third byte
    ch = lst[2]
    if ch not in conv_table:
        print("Warning: data not in conversion table")
        return ''
    return conv_table[ch][shift]

def get_readings(ep):
    while True:
        try: 
            data = ep.read(1000, 500)
            print(data)
        #except KeyboardInterrupt:
        except us.USBError:
            pass
        
    #return data

def get_ep(intf):
    ep = uu.find_descriptor(intf, custom_match = lambda e: uu.endpoint_direction(e.bEndpointAddress) == uu.ENDPOINT_IN)
    return ep

# this method should be considered obselete
def get_dev(vendorID, productID):
    print(vendorID)
    print(productID)
    dev = uc.find(idVendor=vendorID, idProduct=productID)
    if dev is None:
        raise ValueError('DEV is not found')
    return dev

# this is the new method that correctly identifies scanner devices
def get_dev_by_bus_and_address(bus, address):
    print(bus)
    print(address)
    dev = uc.find(bus=bus, address=address)
    if dev is None:
        raise ValueError('No Dev')
    return dev

if __name__ == '__main__':

    # Find our device using the VID (Vendor ID) and PID (Product ID)
    #dev_cdc = get_dev(0x05e0, 0x1701)
    #dev_hid = get_dev(0x05e0, 0x1200)
    
    # run 'lsusb' to identify correct bus and address, example output:
    '''
    Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
    Bus 001 Device 004: ID 046d:c00c Logitech, Inc. Optical Wheel Mouse
    Bus 001 Device 003: ID 8087:0a2a Intel Corp. 
    Bus 001 Device 002: ID 04f2:b59e Chicony Electronics Co., Ltd Chicony USB2.0 Camera
    Bus 001 Device 005: ID 17ef:6018 Lenovo Lenovo Low Profile USB Keyboard
    Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
    '''
    # Bus <bus> Device <address>: ID <vendorID>:<productID> <device description>
    
    dev_cdc = get_dev_by_bus_and_address(0x001, 0x003)
    dev_hid = get_dev_by_bus_and_address(0x001, 0x006)

    # Bugfixing from
    # https://stackoverflow.com/questions/29345325/raspberry-pyusb-gets-resource-busy
    # Disconnect it from kernel
    needs_reattach_cdc = False
    if dev_cdc.is_kernel_driver_active(0):
        needs_reattach_cdc = True
        dev_cdc.detach_kernel_driver(0)
        print("Detached CDC device from kernel driver")

    needs_reattach_hid = False
    if dev_hid.is_kernel_driver_active(0):
        needs_reattach_hid = True
        dev_hid.detach_kernel_driver(0)
        print("Detached HID device from kernel driver")

    # set the active configuration. With no arguments, the first
    # configuration will be the active one
    dev_cdc.set_configuration()
    dev_hid.set_configuration()

    # get an endpoint instance
    cfg_cdc = dev_cdc.get_active_configuration()
    cfg_hid = dev_hid.get_active_configuration()

    intf_cdc = cfg_cdc[(1,0)]
    intf_hid = cfg_hid[(0,0)]

    ep_cdc = get_ep(intf_cdc)
    ep_hid = get_ep(intf_hid)

    assert ep_cdc is not None, "CDC Endpoint not found."
    assert ep_hid is not None, "HID Endpoint not found."

    #print(ep_cdc)
    #print(ep_hid)

    cdc_scanning = threading.Thread(target=get_readings, args=(ep_cdc,))
    hid_scanning = threading.Thread(target=get_readings, args=(ep_hid,))

    cdc_scanning.start()
    hid_scanning.start()

    uu.dispose_resources(dev_cdc)
    uu.dispose_resources(dev_hid)

    if needs_reattach_cdc:
        dev_cdc.attach_kernel_driver(0)
        print('Attached CDC device to kernel driver')
    if needs_reattach_hid:
        dev_hid.attach_kernel_driver(0)
        print('Attached HID device to kernel driver')

