# This script reads an input file of a list of classes and randomly assign one color to each class. The output file is the same filename as the input file - by default - but with its extension changed to .color, the output file is in the format of:

# Color1_Channel1 Color1_Channel2 Color1_Channel3
# Color2_Channel1 Color2_Channel2 Color2_Channel3
# Color3_Channel1 Color3_Channel2 Color3_Channel3
# ...


import numpy as np
import argparse as ap

apr = ap.ArgumentParser()
apr.add_argument("-i", "--input", required=True, help="Input file for list of classes")
apr.add_argument("-o", "--output", help="Output file for list of colours")

args = vars(apr.parse_args())

#print("Printing args:")
#print(args["input"], args["output"])
if args["output"] is None:
    inputArguments = args["input"].split('.')
    # the last element in the list
    print(inputArguments[-1])

    # everything as a list, except the last element
    #print(inputArguments[:-1])

    args["output"] = args["input"].replace(inputArguments[-1], 'color')

#print("Printing args:")
#print(args["input"], args["output"])
def _file_line_count(filename):
    with open(filename) as f:
        #data = f.read()
        #print(data)
        for i, l in enumerate(f):
            pass
        return i+1

nLines = _file_line_count(args["input"])
np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(nLines, 3), dtype="uint8")
#print(COLORS)
np.savetxt(args["output"], COLORS, fmt="%d")
