# Courtesy of:
# https://stackoverflow.com/questions/13005294/measure-the-uniformity-of-distribution-of-points-in-a-2d-square

import numpy as np
from scipy import stats
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import matplotlib.pyplot as plt

class KSTest:

    def __init__(self):
        pass


    def compute_kstest(self, p, distr='uniform'):
        result1 = stats.kstest(MinMaxScaler().fit_transform(p), distr)
        result2 = stats.kstest(StandardScaler().fit_transform(p), distr)
        return result1, result2


if __name__ == '__main__':
    X1 = np.random.normal(size=(1000, 2))
    X2 = np.random.random(size=(1000, 2))
    stds = StandardScaler()
    mms = MinMaxScaler()
    X1 = stds.fit_transform(X1)
    X2 = mms.fit_transform(X2)
    
    kst = KSTest()
    #print(X1)
    plt.scatter(X2[:, 0], X2[:, 1])
    plt.show()

    X1_res1_mm, X1_res1_std = kst.compute_kstest(X1[:,0].reshape(-1, 1), 'uniform')
    X2_res1_mm, X2_res1_std = kst.compute_kstest(X2[:,0].reshape(-1, 1), 'norm')
    X1_res2_mm, X1_res2_std = kst.compute_kstest(X1[:,0].reshape(-1, 1), 'uniform')
    X2_res2_mm, X2_res2_std = kst.compute_kstest(X2[:,0].reshape(-1, 1), 'norm')
    print(X1_res1_mm, X2_res1_mm, X1_res2_mm, X2_res2_mm)
    print(X1_res1_std, X2_res1_std, X1_res2_std, X2_res2_std)
