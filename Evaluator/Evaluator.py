import cv2
import torch
import torchvision
import argparse
from glob import glob
#from plots import plot_image, save_annotation

import sys
if not '/home/william/utilitytools' in sys.path:
    sys.path.append('/home/william/utilitytools')
from Painter.plots import plot_image, save_annotation
from Argparser.Argparser import ParseKwargs

import numpy as np
from scipy import spatial

import LabelConverter.LabelConverter as LCLC
lclc = LCLC.LabelConverter()
roi = LCLC.ROI()

from FileLoader.FileLoader import ModelLoader
ml1 = ModelLoader()

from FileLoader.FileLoader import ImageLoader
il1 = ImageLoader()

from FileLoader.FileLoader import TextLoader
tl1 = TextLoader()

EPSILON = 0.0000000000000001

class Detector(object):
    
    def __init__(self, model_path):
        # Torch hub load
        self.model = ml1.load_weight(fpath=model_path)


    def detect(self, img_path):
        
        img = cv2.imread(img_path)

        #TODO:  Verify img input valid
        
        [H, W, D] = img.shape
        res = self.model(img)
        xyxy_nps = res.xyxy[0].detach().cpu().numpy()
        return xyxy_nps, W, H


    def expand_dim_zero(method):
        def wrapped(*args, **kwargs):
            result = method(*args, **kwargs)
            return np.expand_dims(result, axis=0)
        return wrapped


    def numpy_to_torch(method):
        def wrapped(*args, **kwargs):
            result = method(*args, **kwargs)
            return torch.from_numpy(result)
        return wrapped


    @numpy_to_torch
    @expand_dim_zero
    def list_to_numpy(self, ls):
        return np.array(ls)


    # Input: groundtruth boxes (Human bbs), predicated boxes (Machine bbs)
    # Output: matched grountruth boxes (Intersected Human & Machine boxes
    def detect_bbs(self, gt_boxes, pred_boxes, ind_conf=4, ind_class=5, im_width=None, im_height=None, iou_thresh=0.6):
        matched_gt = []
        pred_coords = np.delete(pred_boxes, [ind_conf, ind_class], 1)

        for bb in gt_boxes:
            cid, xc, yc, w, h = bb
            if im_width and im_height:
                x0, y0, x1, y1 = lclc.xywh2xyxy(xc, yc, w, h, im_width, im_height)
            else:
                raise Exception('Image width and height undefined error')
            
            #print("x0y0x1y1 is: ", x0, y0, x1, y1, cid)
            #print("pred boxes are: ", pred_boxes)

            nearest_bb = roi.find_nearest_bounding_boxes([x0, y0, x1, y1], pred_coords, pred_boxes, n=1)
            #print("Nearest bb is: ", nearest_bb)

            if nearest_bb[ind_class] == cid:
                #np_gt = np.array([x0, y0, x1, y1])
                #np_gt = np.expand_dims(np_gt, axis=0)
                t_gt = self.list_to_numpy([x0, y0, x1, y1])
                #t_gt = torch.from_numpy(np_gt)

                #np_pred = nearest_bb[0:ind_conf]
                #np_pred = np.expand_dims(np_pred, axis=0)
                t_pred = self.list_to_numpy(nearest_bb[0:ind_conf])
                #t_pred = torch.from_numpy(np_pred)
                
                #print("GT and PR are: ", [x0, y0, x1, y1], nearest_bb[0:ind_conf])
                #print("Ground Truth and preds are: ", t_gt, t_pred)

                iou = torchvision.ops.box_iou(t_gt, t_pred)

                if iou > iou_thresh:
                    matched_gt.append(nearest_bb)
                    
        return matched_gt


class Evaluator(object):

    def __init__(self, metric='ROC'):
        self.metric = metric

        #self.s_hits = 0
        #self.s_targets = 0
        #self.s_fps = 0
        #self.s_imgs = 0

    # Input: detector, evaluator, images_path
    # Output: sum_hits, sum_targets, sum_false_positives, sum_image_counts
    def evaluate_imgs(self, detector, imgs_path):
        s_hits = 0
        s_targets = 0
        s_imgs = len(imgs_path)
        s_fps = 0
        #print(imgs_path)
        print("Model confidence at: ", detector.model.conf)
 
        for imp in imgs_path:
            # Human Labor groundtruths
            gt = self.load_groundtruth_txt(imp.replace(".jpg", ".txt"))
            
            # Machine Model predications
            res, W, H = detector.detect(imp)
            if len(res) == 0:
                # No machine predictions found, continue to next image
                n_targets = len(gt)
                s_targets += n_targets
                continue
           
            # The detect_bbs() function to compute bounding boxes of current image as imp for (predication AND groundtruth, subject to iou threshold)
            #print("Groudtruth np array is: ", gt)
            #print("Yolov5 predications is: ", res)

            matched_gt = detector.detect_bbs(gt, res, im_width=W, im_height=H)
            #print("Matched np array is: ", matched_gt)

            n_hits = len(matched_gt)
            s_hits += n_hits

            n_fps = len(res) - len(matched_gt)
            s_fps += n_fps

            n_targets = len(gt)
            s_targets += n_targets

        return s_hits, s_targets, s_fps, s_imgs


    def load_groundtruth_txt(self, txt_path):
        npgt = tl1.load_next_txt(fpath=txt_path)
        return npgt


class ROC(object):

    def __init__(self, confidence_levels=[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]):
        
        self.confs = confidence_levels
        
        ## Detection Rate (recall)
        #self.sum_hits = 0
        #self.sum_targets = 0

        ## False Positive Per Image (precision)
        #self.sum_imgs_count = 0
        #self.sum_false_positive = 0

    
    # Input: list of model confidences
    # Output: list of [recall, fppi] data pairs corresponding to each model confidence value
    def compute_curve(self, detector, evaluator, images_path='./images/*.jpg'):
        ls_evals = []
        for conf in self.confs:
            detector.model.conf = conf
            sum_hits, sum_targets, sum_false_positives, sum_imgs_count = evaluator.evaluate_imgs(detector, images_path) 

            recall = sum_hits / (sum_targets + EPSILON)
            fppi = sum_false_positives / (sum_imgs_count + EPSILON)

            ls_evals.append([recall, fppi])

        return ls_evals


if __name__ == '__main__':

    ap = argparse.ArgumentParser()

    ap.add_argument("-m", "--weight_path", type=str, default="./best.pt", help="Model path, default is ./<object>-<date trained>-<time trained>.pt")
    ap.add_argument("-p", "--image_path", type=str, default="./images", help="Image path, default is ./images")
    ap.add_argument("-n", "--id_to_name", nargs='*', action=ParseKwargs, help="Mapping from class ID to class Name, e.g. -n 0=class1 1=class2 etc...")
    ap.add_argument("-c", "--confidence_span", nargs='+', default=[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9], help="Range of confidence levels to evaluate the predications detecion vs false positive")
    ap.add_argument("-f", "--output_format", type=str, default="%.4f", help="Saved text file metric format, default 4 decimal float")
    ap.add_argument("-t", "--output_text_filename", type=str, default="results.txt", help="Saved evaluation result text file name")

    args = vars(ap.parse_args())

    #model = ml1.load_weight(fpath=args["weight_path"])
    det1 = Detector(model_path=args["weight_path"])

    #img = cv2.imread('./exampleImg.jpg')
    imgs = il1.list_imgs(gpath=args["image_path"])
    #print(type(imgs))
    #print(imgs)

    eva1 = Evaluator()

    roc1 = ROC()

    ls_results = roc1.compute_curve(det1, eva1, images_path=imgs)

    npls = np.array(ls_results)
    
    np.savetxt(args["weight_path"].replace(".pt", "_eval.txt"), npls, fmt='%.4f', delimiter=' ', newline='\n')
