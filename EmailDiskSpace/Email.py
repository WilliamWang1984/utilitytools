###=================================
#### OWNER: **Magnificent Pty. Ltd.**
#### WRITER: Gilbert Eaton, William Wang
#### DATE: July 2019
#### DESC: Email functions for PyBox app.
###=================================

import smtplib

from string import Template

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class Email:

    def __init__(self):
        # Create some members
        self.address = '<your_email_address@your_email.com>'
        self.password = '<your_email_password>'


    def get_contacts(self, filename):
        names = []
        emails = []
        with open(filename, mode='r', encoding='utf-8') as contacts_file:
            for a_contact in contacts_file:
                names.append(a_contact.split()[0])
                emails.append(a_contact.split()[1])
        return names, emails


    def read_template(self, filename):
        with open(filename, 'r', encoding='utf-8') as template_file:
            template_file_content = template_file.read()
        return Template(template_file_content)
