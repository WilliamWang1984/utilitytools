from Functions import Functions
import argparse

def main(functions, path, limit, contact, subject, message):
    print('Starting scheduled Cron...Email Disk Space Check')
    freeDiskSpace = functions.get_freespace_size(path)
   
    # 865551876096 
    # 865551876100
    if int(freeDiskSpace) < int(limit):
        print('Sending Email...')
        functions.send_email(contact, subject, message)

if __name__ == "__main__":
    fun = Functions()
    #pat = '~/rb300/PyBoxQt/images/'
    
    ap = argparse.ArgumentParser()
    ap.add_argument('-p', '--path', type=str, default='/home/william/rb300/PyBoxQt/images/', help='Path or file to check')
    ap.add_argument('-c', '--contacts', type=str, default='/home/william/utilitytools/EmailDiskSpace/contacts.txt', help='List of contacts to Email to')
    ap.add_argument('-s', '--subject', type=str, default='Disk Space Alert', help='Email subject')
    ap.add_argument('-m', '--message', type=str, default='/home/william/utilitytools/EmailDiskSpace/message.txt', help='Email message content')
    ap.add_argument('-l', '--limit', required=True, help='Disk space limit to trigger Email event')
    args = vars(ap.parse_args())

    main(fun, path=args['path'], limit=args['limit'], contact=args['contacts'], subject=args['subject'], message=args['message'])
