###=================================
#### AUTHOR: William Wang
#### DATE: Feb 2019
#### DESC: Generic functions for PyBox app. 
###=================================

#from matplotlib import pyplot as plt
#from keras.preprocessing.image import img_to_array
#from keras.applications import imagenet_utils
#from skimage.morphology import skeletonize
#from PyQt5.QtGui import QPolygonF, QPainter

import numpy as np
import os
import cv2
import copy
import time
import datetime
import scipy.spatial.distance as ssd
import smtplib
import psutil
import socket

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from Email import Email
#from Configuration import Configuration
#from Database import Database
#from MySQL import MySQL

"""
###**CLASS --->**

The Functions file stores generic functions which may be used by any application. 
It contains functions, for example, to get image paths either from a folder of images 
(get_image_paths) or from a structured path (get_structured_image_paths). 

Inherits: ***None***
"""

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"

class Functions:

    # by default, create new empty DB, so that threads such as IOThread which does not need acess actual DB can still call Functions()  
    def __init__(self):
        # Create some members
        self.debug = 0
        self.datetime_format = "%Y-%m-%d %H:%M:%S.%f"
        self.email = Email()
        #self.configuration = Configuration()
        #self.database = Database()
        #self.database = database
        self.sudoPassword = '<your_sudo_password>'


    def get_image_paths(self, paths):
        """
        ####FUNCTION ---> 
        
        **ARGS:**
        
        1. **self**(object) - *the reference to this class.*

        **RETURNS:** 
        
        1. **imagePaths**(string[]) - *a list of paths to images found*

        **DESCR:**

        *Gets the image paths from current folder and returns them*
         
        """
        #array initialise
        imagePaths = []
        #go through all folders
        for name, path in paths.items():
            '''print(name)'''
            #get a list of the files in each folder
            files = next(os.walk(path))[2]
            if (len(files) > 0):
                #for each found file, return the path
                for file in files:
                    #print('Print walking file name:')
                    #print(file)
                    #print('\n')
                    # Only read '.jpg' files, which is original captured images without BBs.
                    if '.jpg' in file:
                        #print('current file is .jpg, add to imagePath')
                        imagePath = os.path.join(path, file)
                        imagePaths.append(imagePath)

        return imagePaths


    '''
    Based on imageType, only return a certain type of image (color RGB or infrared NIR) 
    '''
    def get_structured_image_paths(self, paths, imageType):
        """
        ####FUNCTION ---> 
        
        **ARGS:**
        
        1. **self**(object) - *the reference to this class.*
        2. **paths**(string[]) - *the paths to search for files.*
        3. **imageType**(string) - *the type of file to search for (e.g RGBTop/IRTop).*

        **RETURNS:** 
        
        1. **imagePaths**(string[]) - *a list of paths to images found.*

        **DESCR:**

        *Gets the image paths from a structured folder set (i.e. folders within folders) and returns them.*
         
        """
        #set counter
        imagePaths = []
        #go through all folders
        for name, path in paths.items():
            '''print(name)'''
            #get the list of subdirectoies
            subdirs = [x[0] for x in os.walk(path)]
            for subdir in subdirs:
                #get all files within
                files = next(os.walk(subdir))[2]
                if (len(files) > 2):
                    for file in files:
                        #check the file is the type specified (e.g. RGBTop/IRTop)
                        if (imageType in file):
                            imagePath = os.path.join(subdir, file)
                            imagePaths.append(imagePath)

        return imagePaths



    def datetime_to_timestamp(self, dtAsDatetime):
        return (time.mktime(dtAsDatetime.timetuple())*1e3 + dtAsDatetime.microsecond/1e3)/1e3


    def timestamp_to_datetime(self, tsAsTimestamp):
        return datetime.datetime.fromtimestamp(tsAsTimestamp)


    '''
    Given from_Datetime, to_Datetime of type Python Datetime.Datetime, number of time periods to split, 
    return the list of from_Datetime1, to_Datetime1, from_Datetme2, to_Datetime2, from_Datetime3, to_Datetime3, ... 
    '''
    def datetime_split(self, startDatetime, endDatetime, nSplits):

        xData = []
        queryLists = []

        start = datetime.datetime.strptime(str(startDatetime), DATETIME_FORMAT)
        end = datetime.datetime.strptime(str(endDatetime), DATETIME_FORMAT)

        tsStart = self.datetime_to_timestamp(start)
        tsEnd = self.datetime_to_timestamp(end)
        delta = (tsEnd - tsStart) / nSplits
        
        for i in range(nSplits):
            x = self.timestamp_to_datetime(tsStart + delta * i).strftime(DATETIME_FORMAT)
            xData.append(x)   
        xData.append(self.timestamp_to_datetime(tsEnd).strftime(DATETIME_FORMAT))
        
        queryLists.append(self.timestamp_to_datetime(tsStart).strftime(DATETIME_FORMAT))
        
        for i in range(nSplits-1):
            y = self.timestamp_to_datetime(tsStart + delta * (i+1)).strftime(DATETIME_FORMAT)
            queryLists.append(y)
            queryLists.append(y)
        queryLists.append(self.timestamp_to_datetime(tsEnd).strftime(DATETIME_FORMAT))

        return xData, queryLists

        #for i in range(nSplits):
        #    yield timestamp_to_datetime(tsStart + delta * i).strftime(formatString)
        #yield timestamp_to_datetime(tsEnd).strftime(formatString)
        # ---------------------------------
        # This gives a list: [a, b, c, d, e]

        #yield timestamp_to_datetime(tsStart).strftime(formatString)
        #for i in range(nSplits-1):
        #    yield timestamp_to_datetime(tsStart + delta * (i+1)).strftime(formatString)
        #    yield timestamp_to_datetime(tsStart + delta * (i+1)).strftime(formatString)
        #yield timestamp_to_datetime(tsEnd).strftime(formatString)
        # ---------------------------------
        # This gives a list: [a, b, b, c, c, d, d, e]


    '''
    Given start time (datetimeA), end time (datetimeB), compute delta (B-A) and shift current time (datetimeX) into future time (datetimeX + delta)
    '''
    def datetime_shift(self, datetimeA, datetimeB, datetimeX):
        tsA = self.datetime_to_timestamp(datetimeA)
        tsB = self.datetime_to_timestamp(datetimeB)
        tsX = self.datetime_to_timestamp(datetimeX)
        delta = tsB - tsA
        datetimeY = self.timestamp_to_datetime(tsX + delta).strftime(DATETIME_FORMAT)

        return datetimeY


    '''
    Get free disk space for given path
    '''
    def get_freespace_size(self, queryPath):
        statVFS = os.statvfs(queryPath)
        return statVFS.f_bfree * statVFS.f_bsize


    '''
    Send email for given receivers
    '''
    def send_email(self, contacts, subject, message):
        contactNames, emailAddresses = self.email.get_contacts(contacts)  # read contacts
        messageTemplate = self.email.read_template(message)

        # set up the SMTP server
        smtpHostAndPort = smtplib.SMTP(host='smtp.gmail.com', port=587)
        smtpHostAndPort.starttls()

        print('Email Sender Credential is:')
        print(self.email.address)
        print(self.email.password)

        smtpHostAndPort.login(self.email.address, self.email.password)

        # For each contact, send the email:
        for contactName, emailAddress in zip(contactNames, emailAddresses):
            msg = MIMEMultipart()  # create a message

            # add in the actual person name to the message template
            message = messageTemplate.substitute(RECEIVER_NAME=contactName.title())

            # Prints out the message body for our sake
            print(message)

            # setup the parameters of the message
            msg['From'] = self.email.address
            msg['To'] = emailAddress
            msg['Subject'] = subject

            # add in the message body
            msg.attach(MIMEText(message, 'plain'))

            # send the message via the server set up earlier.
            smtpHostAndPort.send_message(msg)
            del msg

        # Terminate the SMTP session and close the connection
        smtpHostAndPort.quit()


    def set_usbfs_memory_mb(self, size):
        shCmd = "sh -c 'echo %s > /sys/module/usbcore/parameters/usbfs_memory_mb'" % size
        #print(shCmd)
        os.system('echo %s|sudo -S %s' % (self.sudoPassword, shCmd))
        #os.system('cat /sys/module/usbcore/parameters/usbfs_memory_mb')


    '''
    Disconnect wired network to restore Internet for Email or 
    Connect wired network to restore Controllino connections
    '''
    def toggle_wired_network(self, networkName, configOption):
        #sudoPassword = 'Yarnus01'
        ifconfigCmd = 'ifconfig '+networkName+' '+configOption
        os.system('echo %s|sudo -S %s' % (self.sudoPassword, ifconfigCmd))
        print('Wired Network '+configOption)


    '''
    Check if wired network enp19s0 (Controllino network) is up. 
    implementation Courtesy of (ofirule):
    https://stackoverflow.com/questions/17679887/python-check-whether-a-network-interface-is-up
    
    Return True if network is up, False otherwise 
    '''
    def check_wired_network(self, networkName):
        network_addrs = psutil.net_if_addrs().get(networkName) or []
        return socket.AF_INET in [snicaddr.family for snicaddr in network_addrs]


    def get_monitor_resolutions(self):

        listof_monitors_inuse = os.popen("xrandr | grep '*'").read()
        #print(listof_monitors_inuse)
        monitors = listof_monitors_inuse.split('\n')
        #print(monitors)
        resol_x = []
        resol_y = []
        for monitor in monitors:
            monitor_params = monitor.split()
            for param in monitor_params:
                if 'x' in param:
                    #print('Monitor resolution is:')
                    #print(param)
                    xres, yres = param.split('x')
                    resol_x.append(int(xres))
                    resol_y.append(int(yres))
        return resol_x, resol_y


    def get_nearest_element_in_list(self, x, xList):
        return min(xList, key=lambda n: abs(n - x))


    def get_target_resolutions(self, saved_resol_x, saved_resol_y):
        listof_resol_xs, listof_resol_ys = self.get_monitor_resolutions()
        #print("Printing list res ys, y, list res xs, x")
        #print(listof_resol_ys)
        #print(saved_resol_y)
        #print(listof_resol_xs)
        #print(saved_resol_x)

        nearest_resol_x = self.get_nearest_element_in_list(saved_resol_x, listof_resol_xs)
        #nearest_resol_y = self.get_nearest_element_in_list(saved_resol_y, listof_resol_ys)
        removingIndex = listof_resol_xs.index(nearest_resol_x)

        #print(nearest_resol_y)
        #print(nearest_resol_x)
        if len(listof_resol_xs) > 1 and len(listof_resol_ys) > 1:
            #listof_resol_xs.remove(nearest_resol_x)
            del listof_resol_xs[removingIndex]
            del listof_resol_ys[removingIndex]

        return listof_resol_xs, listof_resol_ys


    def get_recent_rejected_image_paths(self, n_rejects):
        punnet_ids = []
        punnet_statuses = []
        imagePaths = []
        # query DB for top nRejects records where punnet.status != Pass
        query_string = "SELECT "+ self.configuration.field['id'] + ', ' + self.configuration.field['status'] + ', ' + self.configuration.field['path'] + " FROM " + self.configuration.tableName['punnet_deep'] + " WHERE " + self.configuration.field['status'] + " != 'Pass'" + \
            " ORDER BY " + self.configuration.field['datetime'] + " DESC LIMIT " + str(n_rejects)
        reject_records = self.database.read_db(self.configuration.databaseName, query_string)
        print("Printing Rejection Records")
        print(reject_records)

        # for each item in the returned list, replace list items' substring .jpg to .jpeg
        for reject_record in reject_records:
            #print(reject_record[0].replace('jpg', 'jpeg', 1))

            imagePaths.append(reject_record[2].replace('.jpg', '.jpeg', 1))
            #imagePaths.append(reject_record[0].replace('.jpg', '.jpeg', 1))
            punnet_ids.append(reject_record[0])
            punnet_statuses.append(reject_record[1])
        return punnet_ids, punnet_statuses, imagePaths
        #return imagePaths 


    def format_qrcode(self, rawCode):
        # Change the low below to embed extra formatting: e.g. convert a hash to a url
        formattedCode = copy.copy(rawCode)
        return formattedCode
    '''
    def timeit(self, method):
        def timed(*args, **kw):
            ts = time.time()
            result = method(*args, **kw)
            te = time.time()    
            
            if 'log_time' in kw:
                name = kw.get('log_name', method.__name__.upper())
                kw['log_time'][name] = int((te - ts) * 1000)
            else:
                print('%r  %2.2f ms' % (method.__name__, (te - ts) * 1000))
            return result    
        return timed
    '''


    def get_scanners_info(self, scnrQueryString):
        stringStream = os.popen('bash lsusb.sh')
        lines = stringStream.readlines()
        scnStrs = []
        for line in lines:
            if scnrQueryString in line:
                
                vendorID = line.split(':')[1][-4:]
                productID = line.split(':')[2][:4]
                bus = line.split(':')[0][4:7]
                address = line.split(':')[0][15:18]
                #print(bus)
                #print(address)

                vendorID_int = self.str2hexint(vendorID)
                productID_int = self.str2hexint(productID)
                bus_int = self.str2decint(bus)
                address_int = self.str2decint(address)

                scnStr = {'vendorID': vendorID_int, 'productID': productID_int, 'bus': bus_int, 'address': address_int}                
                scnStrs.append(scnStr)
        #print(scnStrs)
        return scnStrs


    # is this one line wrapping really necessary?
    def str2hexint(self, inputString):
        return int(inputString, 16)


    def str2decint(self, inputString):
        return int(inputString, 10)
