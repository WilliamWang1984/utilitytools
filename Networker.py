import os
import psutil
import socket

class Networker:

    def __init__(self):
        self.debug = False
        self.sudoPassword = 'YourSudoPassword'

    '''
    Disconnect wired network to restore Internet for Email or
    Connect wired network to restore Controllino connections
    '''
    def toggle_wired_network(self, networkName, configOption):
        ifconfigCmd = 'ifconfig '+networkName+' '+configOption
        os.system('echo %s|sudo -S %s' % (self.sudoPassword, ifconfigCmd))
        print('Wired Network '+configOption)


    '''
    Check if wired network enp19s0 (Controllino network) is up.
    implementation Courtesy of (ofirule):
    https://stackoverflow.com/questions/17679887/python-check-whether-a-network-interface-is-up

    Return True if network is up, False otherwise
    '''
    def check_wired_network(self, networkName):
        network_addrs = psutil.net_if_addrs().get(networkName) or []
        return socket.AF_INET in [snicaddr.family for snicaddr in network_addrs]


    def get_local_ip_address(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        #print(s.getsockname()[0])
        local_ip_addr = s.getsockname()[0]
        s.close()
        return local_ip_addr


    def get_local_ip_address2(self):
        #print(socket.gethostbyname(socket.gethostname()))
        #print(socket.gethostbyname(socket.getfqdn()))
        return socket.gethostbyname(socket.getfqdn())

