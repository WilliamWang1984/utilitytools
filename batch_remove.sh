#!/bin/bash 
########################################################################################
# Always remember to backup!!! Always remember to backup!!! Always remember to backup!!! 
# Especially this is a cleaner script that does batch cleaning !!!
########################################################################################

# Put this file, alongside the files / folders you need to remove, 
# these folders / files to remove should have a similar pattern, e.g. filename_{i} / foldername_{i}
# change the start index (1) and end index (5) in the 'for' line to suit your folder / file name ranges
# change the pattern string 'file_to_remove_'"$i"'.jpg' in the 'rm' line to suit your file name patterns
# change the pattern string 'folder_to_remove_'"$i"'/' in the 'rm -rf' line to suit your folder name patterns

for i in {1..5}
do 
	rm -rf 'folder_to_remove_'"$i"'/' 
	rm 'file_to_remove_'"$i"'.txt'
done 
