#!/bin/bash 
########################################################################################
# Always remember to backup!!! Always remember to backup!!! Always remember to backup!!! 
########################################################################################

# Put this file to one level above the multiple folders that each contain multiple files
# E.g. you have folder1, folder2, folder3.... each containing some images for merging
# the script should reside one level above folder1, folder2, folder3 ...
# in this example, it should be side by side with 'moveParentDirectory'
# folder1 ... folderN should be placed inside of moveParentDirectory and only subfolders
# should be in the moveParentDirectory folder
# the script will move all images in folder1, folder2, folder3 ... sequentially to their 
# parent folder level and delete the original folder1, folder2, folder3...
# renaming the files to folder1_<original filename>.jpg, folder2_<original filename>.jpg
# etc.

cd 'moveParentDirectory'
for i in *
do 
	cd $i
        for j in *
	do
		mv $j $i'_'$j
	done
	mv *.jpg ../
	cd ../
	rm -rf $i
done
