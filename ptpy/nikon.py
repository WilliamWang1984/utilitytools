from ptpy import PTPy

camera = PTPy()

print(camera.get_device_info())

with camera.session():
    camera.initiate_capture()
