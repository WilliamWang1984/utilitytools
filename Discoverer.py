# sudo apt install arp-scan

import subprocess

class Discoverer:

    def __init__(self, timeout=60):
        # Discovery halt after 60 seconds
        self.timeout = timeout


    def get_interfaces(self, netstat_flag='BMRU'):
        cmd = "netstat -i | grep " + netstat_flag
        blist_interfaces = subprocess.check_output((cmd), shell=True, stderr=subprocess.STDOUT)
        #print(list_interfaces)

        # Convert to list of strings
        slist_interfaces = list(str(blist_interfaces.decode('UTF-8')).split('\n'))
        
        # Keep only valid elements of string
        list_interfaces = filter(None, slist_interfaces)

        return list_interfaces


    def get_interface_names(self, list_interfaces):
        intf_names = []
        for intf_str in list_interfaces:
            intf_list = list(filter(None, intf_str.split(' ')))
            intf_names.append(intf_list[0])

        return intf_names


    def get_ethernet_interface_names(self, list_intf_names):
        ether_intf = []
        for intf in list_intf_names:
            if intf.startswith('e'):
                ether_intf.append(intf)
        return ether_intf

    def get_ip_from_mac(self, ethernet_if_name, mac_address):
        cmd = "echo 'YourPassword' | sudo -S arp-scan -I " + ethernet_if_name + " --localnet | grep " + mac_address 
        blist_mapping = subprocess.check_output((cmd), shell=True, stderr=subprocess.STDOUT)
        slist_mapping = list(str(blist_mapping.decode('UTF-8')).split('\n'))

        list_mapping = list(filter(None, slist_mapping))
        if not len(list_mapping) == 1:
            raise Exception('Should return only one IP address to the MAC address')

        #print(list_mapping[0].split(' '))
        #print(list(filter(None, list_mapping[0].split('\t')))[0])
        return list(filter(None, list_mapping[0].split('\t')))[0]

if __name__ == '__main__':
    discoverer1 = Discoverer()
    output1 = discoverer1.get_interfaces()
    intf_names = discoverer1.get_interface_names(output1)
    
    #for i in intf_names:
    #    print(i)

    eth_name = discoverer1.get_ethernet_interface_names(intf_names)
    #for j in eth_names:
    #    print(j)

    adam_ip_6way = discoverer1.get_ip_from_mac(eth_name[0], '74:a5:44:21:8b:79')
    adam_ip_4way = discoverer1.get_ip_from_mac(eth_name[0], '9e:fe:22:34:78:86')
    print(adam_ip_6way)
    print(adam_ip_4way)

