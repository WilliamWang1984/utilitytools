# To split a wildly grown 100000+ files directory into reasonably smaller (1000+) sub folders
# 
# Usage
# python3 reorganize.py -i <str: input_directory> -n <int: number_of_files_each_sub_directory>
# 
# Courtesy of:
# https://askubuntu.com/questions/631220/browsing-folders-with-more-than-100000-images

# Remeber to change the dr and size to suit your needs 

#!/usr/bin/env python3
import subprocess
import os
import shutil
import argparse

#--- set the directory to reorganize below
#dr = "/path/to/directory"
#--- set the number of files/folders per level
#size = 100


ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input_dir", required=True, help="Input directory contains 100000+ files")
ap.add_argument("-n", "--sub_size", type=int, default=2000, help="Sub directory size, default 2000")

args = vars(ap.parse_args())

dr = args["input_dir"]
size = args["sub_size"]

level = 0
def move(fn, drn, level):
    folder = dr+"/"+str(drn)+"_"+str(level)
    if not os.path.exists(folder):
        os.mkdir(folder)
    shutil.move(dr+"/"+f, folder+"/"+f)

while len(os.listdir(dr)) > size:
    level += 1
    fn = 0; drn = 1
    for f in os.listdir(dr):
        if fn < size:
            move(fn, drn, level)
        else:
            fn = 0
            drn += 1
            move(fn, drn, level)
        fn += 1

