#from Painter import Painter
import sys

#if not '/home/william/utilitytools' in sys.path:
#    sys.path.append('/home/william/utilitytools')

#from Painter import Painter
#from Painter import Marker

import numpy as np
import matplotlib.pyplot as plt


import argparse

# Plot 3d scatter point clouds, 
# ns: class_indices
# np_arr: Nx4 array of columes:
#     ns: classes
#     xs: widths
#     ys: heights
#     zs: confidences


ap = argparse.ArgumentParser()

ap.add_argument("-p", "--txtpath", type=str, default="./boxes.txt", help="Path to detected instances boxes txt file, of: class_id(int), width(int), height(int), confidence(float)")
ap.add_argument("-x", "--xaxis", type=str, default="Boundingbox Width", help="Title to the X axis")
ap.add_argument("-y", "--yaxis", type=str, default="Boundingbox Height", help="Title to the Y axis")
ap.add_argument("-z", "--zaxis", type=str, default="Boundingbox Confidence", help="Title to the Z axis")

args = vars(ap.parse_args())

#np_arr = np.loadtxt('boxes_r9_110_5class.txt', ndmin=2)
#np_arr = np.loadtxt('boxes_r9_110_5class.txt', ndmin=2)
np_arr = np.loadtxt(args["txtpath"], ndmin=2)

fig = plt.figure()

ax = fig.add_subplot(projection='3d')

#assert len(xs) == len(ys) == len(zs), raise Exception('Input lengths not equal')
#mrk1 = Marker()

#n_series = len(np.unique(ns))

#ls_inds = [x for x in range(0, n_series)]

#mrks = mrk1.get_markers_chars(ls_inds)
#print(mrks)

# class 0: food, class 1: s1, class 2: s2, class 3: s3, class 4: s4
marks_5class = ['*', '.', '|', '^', 's']
marks_4stage = ['.', '|', '^', 's']

label_dict_5class = {0:'class1', 1:'class2', 2:'class3', 3:'class4', 4:'class5'}
label_dict_4stage = {0:'class2', 1:'class3', 2:'class4', 3:'class5'}

mrks = None
label_dict = None

if '4stage' in args["txtpath"]:
    mrks = marks_4stage
    label_dict = label_dict_4stage
elif '5class' in args["txtpath"]:
    mrks = marks_5class
    label_dict = label_dict_5class

for i, m in enumerate(mrks):
    # filter np_arr_xyc based on ns == i
    np_fil = np_arr[np.where(np_arr[:, 0] == i)]

    clss, xss, yss, cnss = np.hsplit(np_fil, [1, 2, 3])

    ax.scatter(xss, yss, cnss, marker=m, label=label_dict[i])

ax.set_xlabel(args["xaxis"])
ax.set_ylabel(args["yaxis"])
ax.set_zlabel(args["zaxis"])
plt.legend(loc="upper left")
plt.show()
#plt.savefig('3d_point_plot.png')

