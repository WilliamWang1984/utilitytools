import random

class ColorPalette:

    def __init__(self, n_colors=8):
        self.n_colors = n_colors

    def get_random_color(self, pastel_factor=0.5):
        return [(x+pastel_factor)/(1.0+pastel_factor) for x in [random.uniform(0,1.0) for i in [1,2,3]]]

    def color_distance(self, c1,c2):
        return sum([abs(x[0]-x[1]) for x in zip(c1,c2)])

    def generate_new_color(self, existing_colors,pastel_factor=0.5):
        max_distance = None
        best_color = None
        for i in range(0,100):
            color = self.get_random_color(pastel_factor = pastel_factor)
            if not existing_colors:
                return tuple(color)
            best_distance = min([self.color_distance(color,c) for c in existing_colors])
            if not max_distance or best_distance > max_distance:
                max_distance = best_distance
                best_color = color
        return tuple(best_color)

    def generate_listof_colors(self, color_format='uint8'):
        list_colors = []
        for i in range(0, self.n_colors):
        
            c = self.generate_new_color(list_colors, pastel_factor=0.9)
            d = tuple([int(255*x) for x in c])
            if color_format == 'uint8':
                list_colors.append(d)
            elif color_format == 'float':
                list_colors.append(c)
        return list_colors

#Example:

if __name__ == '__main__':
    #To make your color choice reproducible, uncomment the following line:
    #random.seed(10)
    n_colors = 8
    cp1 = ColorPalette(n_colors)
    colors = cp1.generate_listof_colors()

    print("Your colors:",  colors)
