import numpy as np
import cv2
from glob import glob

from PIL import Image, ImageDraw, ImageFont
import math
import os

class Mosiac:


    def __init__(self, n_rows, n_cols, t_width, t_height):
        self.n_rows = n_rows
        self.n_cols = n_cols
        self.t_width = t_width
        self.t_height = t_height


    def get_max_width_height(self, txt_path, img_width=1224, img_height=1224):
        max_width = 0
        max_height = 0
        txts = glob(txt_path)
        for txt in txts:

            tf = open(txt, 'r')
            if os.fstat(tf.fileno()).st_size:
                
                np_mat = np.loadtxt(txt, ndmin=2)
                wmax = max(np_mat[:, 3]) # width max
                hmax = max(np_mat[:, 4]) # height max

                max_width = wmax if wmax > max_width else max_width
                max_height = hmax if hmax > max_height else max_height
            
            tf.close()

        return int(img_width * max_width), int(img_height * max_height)


    def align_tiles(self, np_original, np_painted, img_name):
        
        extracted_img = np.hstack((np_original, np_painted))
        cv2.imwrite(img_name, extracted_img)


    def xywh2xyxy(self, xc, yc, w, h, im_width=1224, im_height=1224):
        x0 = xc - w/2.0
        x1 = xc + w/2.0
        y0 = yc - h/2.0
        y1 = yc + h/2.0

        return [int(x0 * im_width), int(y0 * im_height), int(x1 * im_width), int(y1 * im_height)]

    def plot_box(self, target, label):
        colors = [(144, 238, 144), (240, 128, 128), (173, 216, 230), (153, 153, 0)]
        cls_name = {0: 'c1', 1:'c2', 2:'c3', 3:'c4'}
        painted = target.copy()
        draw = ImageDraw.Draw(painted)
        tw, th = target.size
        draw.rectangle(xy=[0, 0, tw-1, th-1], outline=colors[int(label)], width=5)
        draw.text((1, 1), cls_name[int(label)])
        draw.text((tw-40, th-20), str(th)+"x"+str(tw))

        return painted

if __name__ == '__main__':
    
    mc1 = Mosiac(10, 10, 100, 100)
    
    xw, yh = mc1.get_max_width_height('./images/*.txt')

    mc2 = Mosiac(10, 10, xw, yh)

    #imgs_path = glob('./images/*.jpg')
    txts_path = glob('./images/*.txt')

    #SAVE_SLICE = True
    SAVE_SLICE = False

    max_targets = 0
    for j, txt_path in enumerate(txts_path):

        tf = open(txt_path, 'r')
        #print(txt_path)
        #print(os.fstat(tf.fileno()).st_size)

        if os.fstat(tf.fileno()).st_size:
            
            #print(img_path)
            img_path = txt_path.replace('.txt','.jpg')
            #print(txt_path)
            txt = np.loadtxt(txt_path, ndmin=2)
            
            t_xy = txt[:, [3, 4]]
            #print(t_xy)
            a_list = []
            for xy in t_xy:
                a = xy[0] * xy[1]
                a_list.append(a)
            a_np = np.array(a_list)
            a_np = np.expand_dims(a_np, axis=1)
            #print(a_np)
            np_with_area = np.hstack((txt, a_np))
            
            #print(txt)
            #print(np_with_area)
            
            #img = cv2.imread(img_path)
            img = Image.open(img_path)
            
            n_targets = len(txt)
            max_targets = n_targets if n_targets > max_targets else max_targets
            im_slice = Image.new('RGB', (n_targets * xw, 2 * yh))
            sorted_txt = np_with_area[np_with_area[:, 5].argsort()[::-1]]
            for i, row in enumerate(sorted_txt):
                xyxy = mc2.xywh2xyxy(row[1], row[2], row[3], row[4], im_width=1224, im_height=1224)
                target = img.crop((xyxy[0], xyxy[1], xyxy[2], xyxy[3]))
                painted = mc2.plot_box(target, row[0])
                im_slice.paste(target, (i*xw, 0))
                im_slice.paste(painted, (i*xw, yh))

            if SAVE_SLICE == True:
                draw = ImageDraw.Draw(im_slice)
                font = ImageFont.truetype("arial.ttf", 32)
                draw.text((5, yh-35), txt_path, font=font)

                im_slice.save(img_path.replace('.jpg','_slice.jpg'), 'JPEG')

        tf.close()
   
    n_max_rows = 20
    slices_path = sorted(glob('./slice_imgs/*_slice.jpg'))
    im_stack = Image.new('RGB', (max_targets * xw, n_max_rows * 2 * yh))
    
    for k, sli_path in enumerate(slices_path):
        j = k % n_max_rows   
        im_slic = Image.open(sli_path)
        splitted_path = sli_path.split('_')
        im_stack.paste(im_slic, (0, j*2*yh))
    
        if (k+1) % n_max_rows == 0:
            #im_box = im_stack.getbbox()
            #im_stack = im_stack.crop(im_box)

            im_stack.save(splitted_path[1].split('/')[1]+str(k)+'.jpeg', 'JPEG')
            
            im_stack = Image.new('RGB', (max_targets * xw, n_max_rows * 2 * yh))
        
        if k == len(slices_path)-1:
            #im_box = im_stack.getbbox()
            #im_stack = im_stack.crop(im_box)

            im_stack.save(splitted_path[1].split('/')[1]+str(k)+'.jpeg', 'JPEG')

    #print("tile max width: ", x, "tile max height: ", y)
    
    
