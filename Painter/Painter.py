# Takes in multiple images and align them into M x N grid
# Send out the merged larger image

#from kafka import KafkaProducer
#from kafka import KafkaConsumer

import cv2
from io import BytesIO
from PIL import Image, ImageFont, ImageDraw, ImageEnhance, ImageColor
import numpy as np
import argparse

from datetime import datetime
import time
#import msgpack
#import msgpack_numpy as m
#m.patch()

import json

import random

#from ColorPalette import ColorPalette

#from SharedClass.Timing import Timing
#tim1 = Timing('UNIX_EPOCH')
#from SharedClass.ImageHandler import ImageHandler
#imhan1 = ImageHandler()

import sys


if not '/home/william/utilitytools' in sys.path:
    sys.path.append('/home/william/utilitytools')

from Argparser.Argparser import ParseKwargs

import matplotlib.pyplot as plt

import glob


class ColorPalette:

    def __init__(self, n_colors=8):
        self.n_colors = n_colors

    @staticmethod
    def hex2rgb(h):  # rgb order (PIL)
        return tuple(int(h[1 + i:1 + i + 2], 16) for i in (0, 2, 4))

    def get_random_color(self, pastel_factor=0.5):
        return [(x+pastel_factor)/(1.0+pastel_factor) for x in [random.uniform(0,1.0) for i in [1,2,3]]]

    def color_distance(self, c1,c2):
        return sum([abs(x[0]-x[1]) for x in zip(c1,c2)])

    def generate_new_color(self, existing_colors,pastel_factor=0.5):
        max_distance = None
        best_color = None
        for i in range(0,100):
            color = self.get_random_color(pastel_factor = pastel_factor)
            if not existing_colors:
                return tuple(color)
            best_distance = min([self.color_distance(color,c) for c in existing_colors])
            if not max_distance or best_distance > max_distance:
                max_distance = best_distance
                best_color = color
        return tuple(best_color)

    def generate_listof_colors(self, color_format='uint8'):
        list_colors = []
        for i in range(0, self.n_colors):

            c = self.generate_new_color(list_colors, pastel_factor=0.9)
            d = tuple([int(255*x) for x in c])
            if color_format == 'uint8':
                list_colors.append(d)
            elif color_format == 'float':
                list_colors.append(c)
        return list_colors
    
class Marker:
    # marker dictionary, including all non-numeric matplotlib markers from '.' to '_'
    def __init__(self, marker_dict={0: '.', 1: ',', 2: 'o', 3: 'v', 4: '^', 5: '<', 6: '>', 7: '1', 8: '2', 9: '3', 10: '4', 11: '8', 12: 's', 13: 'p', 14: 'P', 15: '*', 16: 'h', 17: 'H', 18: '+', 19: 'x', 20: 'X', 21: 'D', 22: 'd', 23: '|', 24: '_'}):
        self.markers = marker_dict

    # Default marker dot
    def get_marker_char(self, marker_key=0):
        if isinstance(marker_key, int) and marker_key >= 0 and marker_key <= 24:
            return self.markers[marker_key]
        else:
            # if marker index out of bound, return "None" marker
            return 'None'


    # Default gets 'pixel', 'dot', and 'circle'
    def get_markers_chars(self, markers_keys=[0, 1, 2]):
        ls_markers = []
        for k in markers_keys:
            marker_char = self.get_marker_char(marker_key=k)
            ls_markers.append(marker_char)

        return ls_markers


class Painter:

    def __init__(self, colors=[(0,255,0)], line_thickness=10):
        # Painter parameter
        self.colors = colors
        self.line_thickness = line_thickness
    

    def draw_rect(self, pil_img, rects, drawn_img_name, id_to_name_dict, text_verbosity_level=2, fill=None, fix_outline_thickness=False):
        draw = ImageDraw.Draw(pil_img)
        print("rects are:", rects)
        
        #font = ImageFont.truetype('gulim',15)
        print("colors are:", self.colors)

        for rect in rects:
            #print("self.colors is:", self.colors)
            #print("rect is", rect)
            
            fill = self.colors[int(rect[5])] if fill else None
            width = 5 if fix_outline_thickness else int(self.line_thickness * rect[4])
            draw.rectangle(xy=[rect[0], rect[1], rect[2], rect[3]], outline=self.colors[int(rect[5])], width=width, fill=fill)
            
            font = ImageFont.truetype('/usr/src/app/FreeSansBold.ttf', 20)
            conf = '{0:.2f}'.format(rect[4])
            if text_verbosity_level > 0:
                if text_verbosity_level == 2:
                    draw.text((int(rect[2])+5, int(rect[1])), id_to_name_dict[str(int(rect[5]))] + ': ' + conf, font=font)
                elif text_verbosity_level == 1:
                    draw.text((int(rect[2])+5, int(rect[1])), id_to_name_dict[str(int(rect[5]))], font=font)

        pil_img.save(drawn_img_name)
        print("Drawn image saved to: ", drawn_img_name)
        return pil_img


    def np_gray_to_color(self, np_image):
        return np.stack((np_image,)*3, axis=-1)


    def hex2rgb(self, hex_str):
        return ImageColor.getcolor(hex_str, "RGB")


    def plot_box(self, target, label):
        colors = [(144, 238, 144), (240, 128, 128), (173, 216, 230), (153, 153, 0)]
        cls_name = {0: 's1', 1:'s2', 2:'s3', 3:'s4'}
        painted = target.copy()
        draw = ImageDraw.Draw(painted)
        tw, th = target.size
        draw.rectangle(xy=[0, 0, tw-1, th-1], outline=colors[int(label)], width=5)
        draw.text((1, 1), cls_name[int(label)])
        draw.text((tw-40, th-20), str(th)+"x"+str(tw))

        return painted


    def plot_one_box(self, x, im, color=(128, 128, 128), label=None, line_thickness=3):
        # Plots one bounding box on image 'im' using OpenCV
        #assert im.data.contiguous, 'Image not contiguous. Apply np.ascontiguousarray(im) to plot_on_box() input image.'
        im1 = im

        tl = line_thickness or round(0.002 * (im.shape[0] + im.shape[1]) / 2) + 1  # line/font thickness
        c1, c2 = (int(x[0]), int(x[1])), (int(x[2]), int(x[3]))
        cv2.rectangle(im1, c1, c2, color, thickness=tl, lineType=cv2.LINE_AA)
        if label:
            tf = max(tl - 1, 1)  # font thickness
            t_size = cv2.getTextSize(label, 0, fontScale=tl / 3, thickness=tf)[0]
            c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
            cv2.rectangle(im, c1, c2, color, -1, cv2.LINE_AA)  # filled
            cv2.putText(im1, label, (c1[0], c1[1] - 2), 0, tl / 3, [225, 255, 255], thickness=tf, lineType=cv2.LINE_AA)

        return im1


    def plot_entire_image(self, im, color=(128, 128, 128), line_thickness=5, label=None):
        # x0, y0, x1, y1
        im1 = self.plot_one_box([0, 0, im.shape[1], im.shape[0]], im, color=color, label=label, line_thickness=line_thickness)

        return im1


    def cv_to_pil(self, input_img):
        img = cv2.cvtColor(input_img, cv2.COLOR_BGR2RGB)
        output_img = Image.fromarray(img)
        
        return output_img


    def pil_to_cv(self, input_img):
        return np.asarray(input_img)


    def paint_random(self, im_width=100, im_height=100, max_pixels=255, n_channels=3):
        return np.random.randint(max_pixels, size=(im_height, im_width, n_channels), dtype=np.uint8)


    def paint_random_channels(self, im_width=100, im_height=100, max_pixel_red=255, max_pixel_green=255, max_pixel_blue=255):
        rand_red = self.paint_random(im_width, im_height, max_pixels=max_pixel_red, n_channels=1)
        rand_green = self.paint_random(im_width, im_height, max_pixels=max_pixel_green, n_channels=1)
        rand_blue = self.paint_random(im_width, im_height, max_pixels=max_pixel_blue, n_channels=1)

        # Follow opencv convention of bgr
        return np.dstack((rand_blue, rand_green, rand_red))

    # Plot 3d scatter point clouds, 
    # ns: class_indices
    # np_arr: Nx4 array of columes:
    #     ns: classes
    #     xs: widths
    #     ys: heights
    #     zs: confidences
    def plot_3d(self, plt, np_arr, x_label='X Axis', y_label='Y Axis', z_label='Z Axis'):
        fig = plt.figure()
        
        print("Passed fig")

        ax = fig.add_subplot(projection='3d')
        
        print("Passed fig and ax...")

        #assert len(xs) == len(ys) == len(zs), raise Exception('Input lengths not equal')
        mrk1 = self.Marker()

        n_series = len(np.unique(ns))

        ls_inds = [x for x in range(0, n_series)]

        mrks = mrk1.get_markers_chars(ls_inds)
        print(mrks)

        for i, m in enumerate(mrks):
            # filter np_arr_xyc based on ns == i
            np_fil = np_arr[np.where(np_arr[:, 0] == i)]

            clss, xss, yss, cnss = np.hsplit(np_fil, [1, 2, 3])

            ax.scatter(xss, yss, cnss, marker=m)

        plt.show()
        #plt.savefig('3d_point_plot.png')


    # Hstack original images left, painted images right 
    def stack_pairs(self, ori_imgs_path, ori_imgs_replacing_str='images', pnt_imgs_replaced_str='painted', stk_imgs_replaced_str='./'):
        ls_oris = glob.glob(ori_imgs_path)
        #ls_pnts = glob.glob(pnt_imgs_path)
        for ori_img_path in ls_oris:
            
            ori_img = cv2.imread(ori_img_path)
            pnt_img_path = ori_img_path.replace(ori_imgs_replacing_str, pnt_imgs_replaced_str)
            pnt_img = cv2.imread(pnt_img_path)

            res_img_path = ori_img_path.replace(ori_imgs_replacing_str, stk_imgs_replaced_str)

            #print(ori_img_path)
            #print(pnt_img_path)

            stacked_img = np.hstack((ori_img, pnt_img))
            cv2.imwrite(res_img_path, stacked_img)


if __name__ == '__main__':

    ap = argparse.ArgumentParser()
    
    ap.add_argument("-n", "--n_colors", type=int, default=8, help="Number of colors to use")
    ap.add_argument("-k", "--line_thickness", type=int, default=10, help="Brush line thickness")
    ap.add_argument("-c", "--color_palette", nargs='+', default=[], help="List of drawing colors, (-c '255,0,0' '0,255,0' '0,0,255')")

    ap.add_argument("-m", "--id_to_name", nargs='*', action=ParseKwargs, help="Mapping from class ID to class Name. e.g. -m 0=class1 1=class2")
    ap.add_argument("-t", "--text_verbosity_level", type=int, default=2, help="Whether or not to write text next to object bounding boxes, choices are name_and_confidence(2), name(1), none(0)") 
    args = vars(ap.parse_args())

    list_colors = []
    for j in range(len(args["color_palette"])):
        list_colors.append(eval(args["color_palette"][j]))
    
    cp1 = ColorPalette(args["n_colors"])
    if len(list_colors) == 0:
        list_colors = cp1.generate_listof_colors()

    #print(list_colors)

    # Initalize class instance
    painter1 = Painter(colors=list_colors, line_thickness=args["line_thickness"])

    #for ori_img_msg, pred_msg in zip(img_consumer, pred_consumer):

    #draw_img = painter1.draw_rect(pil_img=ori_img_value, rects=pred_value, drawn_img_name='/usr/src/app/painted_imgs/'+pred_key+'.jpg', id_to_name_dict=args["id_to_name"], text_verbosity_level=args["text_verbosity_level"], fix_outline_thickness=True)


    #randrgb = painter1.paint_random_channels(1000, 800, max_pixel_red=10, max_pixel_green=88, max_pixel_blue=118)
    #rand3 = painter1.paint_random(1000, 800)
    #cv2.imshow("", rand3)
    #cv2.waitKey(0)

    # Inputs are: 1. path to original images (positional), 2. to be replaced original image input string (keyword), 3. painted image output string after replacement (keyword), 4. result image output string after replacement (keyword)
    painter1.stack_pairs('./images/*.jpg')


