# Plotting utils

from copy import copy
from pathlib import Path

import cv2
import math
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
import torch
import yaml
from PIL import Image, ImageDraw, ImageFont

# Settings
matplotlib.rc('font', **{'size': 11})
matplotlib.use('Agg')  # for writing to files only


class Colors:
    # Ultralytics color palette https://ultralytics.com/
    def __init__(self, hex_color=[]):
        # hex = matplotlib.colors.TABLEAU_COLORS.values()
        #hex = ('00FF00', 'FF0000', '0000FF', 'FFD700', '90EE90', 'F08080', 'ADD8E6', '999900')
        #hex_color = ('90EE90', 'F08080', 'ADD8E6', '999900', '00FF00', 'FF0000', '0000FF', 'FFD700')
        
        # (green, red, blue, gold, light green, light coral, light blue, yellow)
        # hex = ('FF3838', 'FF9D97', 'FF701F', 'FFB21D', 'CFD231', '48F90A', '92CC17', '3DDB86', '1A9334', '00D4BB',
        #        '2C99A8', '00C2FF', '344593', '6473FF', '0018EC', '8438FF', '520085', 'CB38FF', 'FF95C8', 'FF37C7')
        self.palette = [self.hex2rgb('#' + c) for c in hex_color]
        self.n = len(self.palette)

    def __call__(self, i, bgr=False):
        c = self.palette[int(i) % self.n]
        return (c[2], c[1], c[0]) if bgr else c

    @staticmethod
    def hex2rgb(h):  # rgb order (PIL)
        return tuple(int(h[1 + i:1 + i + 2], 16) for i in (0, 2, 4))


#colors = Colors()  # create instance for 'from utils.plots import colors'


def plot_one_box(x, im, color=(128, 128, 128), label=None, line_thickness=3):
    # Plots one bounding box on image 'im' using OpenCV
    #assert im.data.contiguous, 'Image not contiguous. Apply np.ascontiguousarray(im) to plot_on_box() input image.'
    tl = line_thickness or round(0.002 * (im.shape[0] + im.shape[1]) / 2) + 1  # line/font thickness
    c1, c2 = (int(x[0]), int(x[1])), (int(x[2]), int(x[3]))
    cv2.rectangle(im, c1, c2, color, thickness=tl, lineType=cv2.LINE_AA)
    if label:
        tf = max(tl - 1, 1)  # font thickness
        t_size = cv2.getTextSize(label, 0, fontScale=tl / 3, thickness=tf)[0]
        c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
        cv2.rectangle(im, c1, c2, color, -1, cv2.LINE_AA)  # filled
        cv2.putText(im, label, (c1[0], c1[1] - 2), 0, tl / 3, [225, 255, 255], thickness=tf, lineType=cv2.LINE_AA)


def plot_image(image, preds, info=None, fname='images.jpg', names=None, max_size=1224, conf_thresh=0,
               cls_names=['class1', 'class2', 'class3', 'class4', 'class5', 'class6', 'class7', 'class8'], color_palette=[], 
               first_column_class_id=False, plot_original=False):
    
    oriimg = image.copy()
    # Plot image grid with labels
    colors = Colors(color_palette)

    if isinstance(image, torch.Tensor):
        image = image.cpu().float().numpy()
    if isinstance(preds, torch.Tensor):
        preds = preds.cpu().numpy()

    # un-normalise
    if np.max(image[0]) <= 1:
        image *= 255

    tl = 3  # line thickness
    tf = max(tl - 1, 1)  # font thickness
    h, w, _ = image.shape  # height, width, _

    # Check if we should resize
    scale_factor = max_size / max(h, w)
    if scale_factor < 1:
        h = math.ceil(scale_factor * h)
        w = math.ceil(scale_factor * w)

    # preds - numpy array: [xmin, ymin, xmax, ymax, confidence, class, name]
    if len(preds) > 0:
        if not first_column_class_id:
            boxes = preds[:, 0:4]
            classes = preds[:, 5].astype('int')
            confs = preds[:, 4]
        elif first_column_class_id:
            boxes = preds[:, 1:5]
            classes = preds[:, 0].astype('int')
            confs = np.ones((len(preds), 1))

        if boxes.shape[1]:
            if boxes.max() <= 1.01:  # if normalized with tolerance 0.01
                boxes[[0, 2]] *= w  # scale to pixels
                boxes[[1, 3]] *= h
            elif scale_factor < 1:  # absolute coords need scale if image scales
                boxes *= scale_factor

        for j, box in enumerate(boxes):
            cls = int(classes[j])
            color = colors(cls)
            if confs[j] > conf_thresh:  # 0.25 conf thresh
                label = '%s %.3f' % (cls_names[cls], confs[j])
                #label = '%s' % (cls_names[cls])
                plot_one_box(box, image, label=label, color=color, line_thickness=tl)

    # Draw image filename labels
    if info:
        t_size = cv2.getTextSize(info, 0, fontScale=tl / 3, thickness=tf)[0]
        cv2.putText(image, info, (5, t_size[1] + 5), 0, tl / 3, [220, 220, 220], thickness=tf,
                    lineType=cv2.LINE_AA)

    # Image border
    cv2.rectangle(image, (0, 0), (w,  h), (255, 255, 255), thickness=3)

    if plot_original == True:
        image = np.hstack((oriimg, image))

    if fname:
        cv2.imwrite(fname, cv2.cvtColor(image, cv2.COLOR_BGR2RGB))  # cv2 save
    return image


def save_annotation(path, preds, img_height, img_width):
    if preds[:, :3].max() > 1.01:
        preds[:, [0, 2]] = preds[:, [0, 2]] / img_width
        preds[:, [1, 3]] = preds[:, [1, 3]] / img_height

    df = pd.DataFrame(preds, columns=['x1', 'y1', 'x2', 'y2', 'score', 'cls'])

    df['cx'] = (df['x1'] + df['x2']) / 2.0
    df['cy'] = (df['y1'] + df['y2']) / 2.0
    df['w'] = df['x2'] - df['x1']
    df['h'] = df['y2'] - df['y1']
    df['cls'] = df['cls'].astype('int')

    df.to_csv(path_or_buf=path, columns=['cls', 'cx', 'cy', 'w', 'h'], sep=' ', header=False, index=False)


