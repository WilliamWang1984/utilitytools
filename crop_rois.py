import cv2
import os
from glob import glob
import numpy as np
import multiprocessing 
from multiprocessing import Pool
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True, help="Input files path, default .jpg")
ap.add_argument("-o", "--output", required=True, help="Output files path")
#ap.add_argument("-t", "--type", required=False, default=".png", help="Output image format (default .png)")

args = vars(ap.parse_args())
windowSize = 64
windowStep = 100
sourceFilesPath = './'+args["input"]+'/*.jpg' # fAnoGAN_training_punnets
destinationPath = './'+args["output"]+'/' # fAnoGAN_training_rois
roiExtension = '.png'

def strip_file_paths_and_extension(fileName):
	splittedFilename = fileName.split('/')
	return splittedFilename[-1].split('.')[0]

def get_roi_filename(coreName, sclFactor, hInd, wInd, rExt):
	def _convert_scale(sclFactor):
		return str(sclFactor).replace('.', 'p')
	def _convert_index(hInd):
		return str(hInd)
	scalePiece = _convert_scale(sclFactor)
	hIndPiece = _convert_index(hInd)
	wIndPiece = _convert_index(wInd)

	return destinationPath+'_'.join([coreName, scalePiece, hIndPiece, wIndPiece])+roiExtension

def splice_length(length, segLength, segInterval):
	return np.arange(0, length-segLength, segInterval).tolist()

def crop_roi_multiple_scale(imgResize, imgCorename, scaleFactor):
	nRois = 0
	(imHeight, imWidth, _) = imgResize.shape
	hIndices = splice_length(imHeight, windowSize, windowStep)
	wIndices = splice_length(imWidth, windowSize, windowStep)
	hCnt = 0
	wCnt = 0
	for hIndex in hIndices:
		for wIndex in wIndices:
			roiFilename = get_roi_filename(imgCorename, scaleFactor, hIndex, wIndex, roiExtension)
			cv2.imwrite(roiFilename, imgResize[hIndex:hIndex+windowSize, wIndex:wIndex+windowSize])
			wCnt += 1
			nRois += 1
		hCnt += 1	
	return nRois

def crop_roi(img, imgName):
	nRois = 0
	scaleFactors = [0.500000]#[0.333333, 0.666667]
	
	imgCorename = strip_file_paths_and_extension(imgName)

	for scaleFactor in scaleFactors:
		imgResize = cv2.resize(img, None, fx=scaleFactor, fy=scaleFactor)
		nRoi1 = crop_roi_multiple_scale(imgResize, imgCorename, scaleFactor)
		nRois += nRoi1	

	return nRois

def crop_roi_fname(imgName):
	image = cv2.imread(imgName)
	crop_roi(image, imgName)

if __name__ == '__main__':
	sumROIs = 0
	useParallel = True
	if not useParallel:	
		'''
		for imageName in glob('./fAnoGAN_training_punnets/*.jpg'):
			image = cv2.imread(imageName)
			nROIs = crop_roi(image, imageName)
			#print(imageName)
			#cv2.imshow("Debug",image)
			#cv2.waitKey(0)
			sumROIs += nROIs
		'''
	elif useParallel:

		listofAllFiles = glob(sourceFilesPath)
		with Pool(16) as p:
			p.map(crop_roi_fname, listofAllFiles)
	else:
		raise Exception('Unknow execution option!')
	print('a total of %i extracted' % sumROIs)
