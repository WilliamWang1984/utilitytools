# Author: William WANG
# Organization: Griffith IIIS

# Courtesy of:
#https://medium.com/pythonhive/python-decorator-to-measure-the-execution-time-of-methods-fa04cb6bb36d

# TextNumpyer Class used to read from and write to text, into numpy array or as .txt file:
# if .txt file is empty, returned numpy array is None

# @read_to_np2d
# To use,
# from TextNumpyer import TextNumpyer 
# define textnumpyer = TextNumpyer(), 
# 
# @textnumpyer.read_to_np2d
# def your_function_to_read_file(...):
#     ...
# 
# if __name__ == __main()__:
#        your_function_to_read_file(...)

import os
import numpy as np
from collections import Counter
from glob import glob
import sys

if not '/home/william/utilitytools' in sys.path:
    sys.path.append('/home/william/utilitytools')

from LabelConverter.LabelConverter import LabelConverter
lclc1 = LabelConverter()

import argparse

class TextNumpyer:

    def __init__(self, ndmin=2):
        self.ndmin = ndmin # legal values are 0 (can read empty txt), 1 (all number to 1D long vector), 2 (read txt as 2D array)


    def check_empty(method):
        def wrapped(*args, **kw):
            # initialize result to None
            result = None

            txtname = kw.get('txtpath')
            tf = open(txtname, 'r')

            # if file is not empty
            if os.fstat(tf.fileno()).st_size:

                # replace initial result to file content
                result = method(*args, **kw)
                
            tf.close()

            return result

        return wrapped
    

    @check_empty
    def read_txt_to_numpy(self, txtpath=None, ndmin=2):
        return np.loadtxt(txtpath, ndmin=ndmin)
        

    def occurences(self, np_arr=None, instance_ind=0):
        return Counter(x[instance_ind] for x in np_arr)


    # Convert class, x_c, y_c, w, h, conf  to   class, W, H, conf 
    def xywhs_to_whconfs(self, np_arr=None, pdict={'cind':0, 'xcenter':1, 'ycenter':2, 'width':3, 'height':4, 'conf':5, 'im_width':1440, 'im_height':1080}):
        
        ls_whconfs = []
        lclass = None
        lcoord = None
        lconf = None
        
        # remove and stash class, conf colume in np_arr
        if np_arr is not None:
            lcoord, lconf = np.hsplit(np_arr, [5])

        # pass np_arr to xywhs2xyxys()
        xyxys = lclc1.xywhs2xyxys(lcoord, pdict=pdict)

        # compute W = x1 - x0, H = y1 - y0
        cls, x0s, y0s, x1s, y1s = np.hsplit(xyxys, [1, 2, 3, 4])
        Ws = x1s - x0s
        Hs = y1s - y0s

        # hstack class, W, H, conf
        if lconf is not None:
            ls_whconfs = np.hstack((cls, Ws, Hs, lconf))

        return ls_whconfs


if __name__ == '__main__':


    ap = argparse.ArgumentParser()

    ap.add_argument("-p", "--txtpath", type=str, default='./labels', help="Path to .txt label files, format should be class_id, x, y, w, h, confidence")

    args = vars(ap.parse_args())

    tn1 = TextNumpyer()

    #exnp = tn1.read_txt_to_numpy(txtpath='./example.txt')
    #exnp1 = tn1.read_txt_to_numpy(txtpath='./example1.txt')

    #cnts = tn1.occurences(exnp)
    #cnts1 = tn1.occurences(exnp1)
    #print(cnts+cnts1)
    
    #ls_txts = glob('./labels/*.txt')
    ls_txts = glob(args["txtpath"]+'/*.txt')

    sum_cnts = Counter()

    ls_stack_nps = np.array([], dtype=np.float64).reshape(0,4)

    for txt in ls_txts:
        txtnp = tn1.read_txt_to_numpy(txtpath=txt)
        cnts = tn1.occurences(txtnp)

        sum_cnts += cnts

        ls_mer = tn1.xywhs_to_whconfs(txtnp)
        ls_stack_nps = np.vstack((ls_stack_nps, ls_mer))

    ls_cnts = list(sum_cnts.items())
    np.savetxt(args["txtpath"]+'_'+'counts.txt', np.array(ls_cnts), fmt='%d %d')
    np.savetxt(args["txtpath"]+'_'+'boxes.txt', ls_stack_nps, fmt='%d %d %d %.4f')
