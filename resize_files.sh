#!/bin/bash 
# Courtesy of https://askubuntu.com/questions/271776/how-to-resize-an-image-through-the-terminal
########################################################################################
# Always remember to backup!!! Always remember to backup!!! Always remember to backup!!! 
########################################################################################

# Put this file into the destination folder containing multiple images
# Adjust the resize parameter {-resize <N%> (where N% is the percentage of the original image resolution for both x and y)} if necessary 
# You may change the second "{}" expression (i.e. "{}" at the end of the line) to save the resized images into a different name. 
# You may also change the max traverse depth {-maxdepth N (N being a natural number)} to process subfolders and subfolders of subfolders etc. 
# This resize_files.sh will attempt to resize every image 
# !!! Note the original files may be lost and replaced by the resized image, so always remember to do a backup before running !!!

find . -maxdepth 1 -iname "*.jpg" | xargs -L1 -I{} convert -resize 85% "{}" "{}"
