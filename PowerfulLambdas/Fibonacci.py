
class Fibonacci:
    
    def __init__(self):
        pass

    def compute(self, fn):
        fib = lambda f: f if f<=1 else fib(f-1) + fib(f-2)
        return fib(fn)

    def generate(self, ffirst, flast):
        return [fib1.compute(i) for i in range(ffirst, flast)]

if __name__ == '__main__':
    fib1 = Fibonacci()
    print(fib1.generate(ffirst=10, flast=20))

