
class Factorial:
    
    def __init__(self):
        pass

    def compute(self, fn):
        fac = lambda f: f if f <= 1 else f * fac(f - 1)
        return fac(fn)

    def generate(self, ffirst, flast):
        return [self.compute(i) for i in range(ffirst, flast)]

if __name__ == '__main__':
    fac1 = Factorial()
    print(fac1.generate(ffirst=3, flast=10))

