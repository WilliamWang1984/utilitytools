
class Quicksort:
    
    def __init__(self):
        pass

    def sort(self, l):
        qsort = lambda l : l if len(l)<=1 else qsort([x for x in l[1:] if x < l[0]]) + [l[0]] + qsort([x for x in l[1:] if x >= l[0]])

        return qsort(l)

if __name__ == '__main__':
    qs1 = Quicksort()
    ll = qs1.sort([4, 5, 8, 1, 9, 2, 7, 0, 3, 6])
    print(ll)
