import os
from glob import glob
import cv2
import numpy as np
import argparse
from PIL import Image, ImageDraw, ImageFont

### Custome Class imports
import sys

if not '/home/william/utilitytools' in sys.path:
    sys.path.append('/home/william/utilitytools')

from TextNumpyer.TextNumpyer import TextNumpyer
from LabelConverter.LabelConverter import LabelConverter
from FilePathSplitter.FilePathSplitter import FilePathSplitter
from Painter.Painter import Painter
from ImageFormatConverter.ImageFormatConverter import ImageFormatConverter

tn1 = TextNumpyer()
lc1 = LabelConverter()
fps1 = FilePathSplitter(delimiter='.')
ptr1 = Painter()
ifc1 = ImageFormatConverter()

class Cropper:

    def __init__(self, im_width=1440, im_height=1080):
        self.im_width = im_width
        self.im_height = im_height


    # x0, y0, x1, y1
    def mask_rects(self, img, rects=[[0, 0, 100, 100],[980, 0, 1440, 100]]):
        for i, rect in enumerate(rects):
            print(rect)
            img = cv2.rectangle(img, rect, (0,0,0), -1)

        return img


    def crop_origin(self, img, x_radius=100, y_radius=100):
        x_origin = int(self.im_width / 2.0)
        y_origin = int(self.im_height / 2.0)

        x_min = x_origin - x_radius
        x_max = x_origin + x_radius
        y_min = y_origin - y_radius
        y_max = y_origin + y_radius

        #print(y_min, y_max, x_min, x_max)

        return img[y_min:y_max, x_min:x_max]


    def padding_with_frame(self, img, frm, custom_ind_x_tl=None, custom_ind_y_tl=None):
        im_height, im_width, im_depth = img.shape
        fm_height, fm_width, fm_depth = frm.shape
        
        #print(im_height, fm_height)

        if fm_height < im_height or fm_width < im_width:
            raise Exception("Background image size less than front image size, check images in ./backgrounds and ./images")

        padded_img = frm.copy()

        #  compute frm central location
        cen_x_frm, cen_y_frm = int(fm_width / 2.0), int(fm_height / 2.0)
        #  compute frm top-left corner for paste
        cen_x_img, cen_y_img = int(im_width / 2.0), int(im_height / 2.0)
        ind_x_frm, ind_y_frm = int(cen_x_frm - cen_x_img), int(cen_y_frm - cen_y_img)

        if custom_ind_x_tl is None or custom_ind_y_tl is None:
            padded_img[ind_y_frm:ind_y_frm+im_height, ind_x_frm:ind_x_frm+im_width] = img
        else:
            padded_img[custom_ind_y_tl:custom_ind_y_tl+im_height, custom_ind_x_tl:custom_ind_x_tl+im_width] = img

        return padded_img


    def padding_to_square(self, img):
        
        # 1. obtain img width and height
        # 2. compare width and height, find max
        # 3. padding the other non-max dimension to max
        
        H, W, D = img.shape
        hw = [H, W]
        ind, max_dim, min_dim = hw.index(max(hw)), max(hw), min(hw)
        #print(ind, max_dim)

        margin = int((max_dim - min_dim) / 2.0)

        if ind == 0:
            # slim image, padding along x axis
            img1 = cv2.copyMakeBorder(img, 0, 0, margin, margin, cv2.BORDER_CONSTANT, value=[0,0,0])
        elif ind == 1:
            # fat image, padding along y axis
            img1 = cv2.copyMakeBorder(img, margin, margin, 0, 0, cv2.BORDER_CONSTANT, value=[0,0,0])
            #img1 = cv2.copyMakeBorder(img, margin, margin, 0, 0, cv2.BORDER_ISOLATED)
            #img1 = cv2.copyMakeBorder(img, margin, margin, 0, 0, cv2.BORDER_REFLECT_101)

        return img1


    def crop_by_txt(self, img, txt_pathname, save_bbox_crop=True, stack_painted=True):
        # read txt 
        # crop images according to txt ==> crops
        # for each crop:
        #     painted_crop = paint(crop)
        # res = (hstack crops to stripe) 
        # res_painted = (hstack painted_crops to stripe)
        # if save_bbox_crop:
        #     create folder of image_name
        #     for each crop in list_of_crops:
        #         save_crop(crop, image_name_crop-index.jpg)
        #
        # if stack_painted:
        #     res = (vstack res and res_painted)
        #     
        res = img
        img = img.copy()
        #print("Processing file: ", txt_pathname)
        np1 = tn1.read_txt_to_numpy(txtpath=txt_pathname, ndmin=2)
        print("Processing: ", txt_pathname, ", # of objs: ", len(np1))

        # np1:
        # <class_id> x_c, y_c, w, h

        # xywh2xyxy

        if np1 is None:
            return np1, 0

        nps = [] if np1 is None else np1
        
        ls_crops = []

        max_crop_h = 0
        max_crop_w = 0

        for row in nps:
            x0, y0, x1, y1 = lc1.xywh2xyxy(row, img_size=(img.shape[0], img.shape[1]))
           
            crop_h = y1 - y0
            crop_w = x1 - x0

            max_crop_h = crop_h if crop_h > max_crop_h else max_crop_h
            max_crop_w = crop_w if crop_w > max_crop_w else max_crop_w 
            
            crop_area = crop_w * crop_h
            ls_crops.append([x0, y0, x1, y1, crop_area])

        np_crops = np.array(ls_crops)

        if np_crops.ndim == 2:
            sort_crops = np_crops[np_crops[:, 4].argsort()[::-1]]
             
            #print(sort_crops)
            n_crops = len(sort_crops)

            im_slice = Image.new('RGB', (n_crops * max_crop_w, max_crop_h))
            pt_slice = Image.new('RGB', (n_crops * max_crop_w, max_crop_h))

            #print("Number of crops: ", n_crops)
            for i, crop_coords in enumerate(sort_crops):
                

                xmin, ymin, xmax, ymax = crop_coords[0:-1]
                #print(xmin, ymin, xmax, ymax)

                #print("img shape: ", img.shape)

                crop_cv = img[ymin:ymax, xmin:xmax]

                #print(i, " crop_cv shape: ", crop_cv.shape)
                crop_pil = ptr1.cv_to_pil(crop_cv)
                #print(crop.shape)

                #print("Pasting ", i, "-th crop, canvas size: ", n_crops * max_crop_w)
                im_slice.paste(crop_pil, (i * max_crop_w, 0))
                
                if save_bbox_crop == True:
                    # create folder in the name of the image

                    #print("Folder path of txt path: ", txt_pathname)
                    folder_path = fps1.strip_file_extension(txt_pathname)
                    #print("Folder path to create: ", folder_path)
                    if not os.path.exists(folder_path):
                        os.mkdir(folder_path, mode = 0o755)
                    cv2.imwrite(folder_path+'/crop_'+str(i)+'.jpg', crop_cv)
                
                # paint each crop with info
                color = (128, 84, 167)
                line_thickness = 5
                
                text = "i, "+str(crop_coords[2]-crop_coords[0])+' x '+str(crop_coords[3]-crop_coords[1])

                painted_cv = ptr1.plot_entire_image(crop_cv.copy(), color=color, line_thickness=line_thickness, label=text)
                
                #print("Cropped shape: ", crop_cv.shape)
                #print("Painted shape: ", painted_cv.shape)

                #cv2.imshow("crop", crop_cv)
                #cv2.imshow("paint", painted_cv)
                painted_pil = ptr1.cv_to_pil(painted_cv)

                # paste painted crop to pt_slice
                pt_slice.paste(painted_pil, (i * max_crop_w, 0)) 
            
            im_slice_cv = ptr1.pil_to_cv(im_slice)

            #print("stack painted: ", stack_painted)
            if stack_painted == True:
                # vertical stack im_slice and pt_slice, return as result
                pt_slice_cv = ptr1.pil_to_cv(pt_slice)

                return np.vstack((im_slice_cv, pt_slice_cv)), len(ls_crops)

            return im_slice_cv, len(ls_crops)

        return None, len(ls_crops)


if __name__ == '__main__':
    
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--src_path", type=str, default="./images/*.jpg", help="Path to source image files")
    ap.add_argument("-q", "--dst_path", type=str, default="./images/cropped/", help="Path to destination image files")
    ap.add_argument("-x", "--mode", type=str, default="crop", help="Image processing mode (crop / crops / mask / square / frame)")
    ap.add_argument("-w", "--crop_width", type=int, default=350, help="Image crop x radius, cropped image width is 2 * crop_width")
    ap.add_argument("-l", "--crop_length", type=int, default=500, help="Image crop y radius, cropped image height is 2 * crop_height")
    #ap.add_argument("-r", "--rects", nargs='+', default=[[0,0,420,630],[1082,0,1440,610],[0,860,400,1080],[1100,845,1440,1080]], help="mask mode list of rectangle coordinates in [[x0, y0, x1, y1],[x0, y0, x1, y1], ... ]")
    ap.add_argument("-r", "--rects", nargs='+', default=[[397, 217, 640, 640]], help="mask mode list of rectangle coordinates in [[x0, y0, w, h] ... ]")
    ap.add_argument("-m", "--origin_length", type=int, default=1224, help="Input image original length (height)")
    ap.add_argument("-n", "--origin_width", type=int, default=1224, help="Input image original width")

    ap.add_argument("-s", "--save_bbox_crop", dest="save_bbox_crop", action="store_true", help="Save the cropped image patches to subfolder of the image name")
    ap.add_argument("-k", "--no_save_bbox_crop", dest="save_bbox_crop", action="store_false", help="Do not save cropped image patches")
    ap.set_defaults(save_bbox_crop=True)

    ap.add_argument("-t", "--stack_painted", dest="stack_painted", action="store_true", help="Stack painted stripe image to the bottom of the original stripe image")
    ap.add_argument("-o", "--no_stack_painted", dest='stack_painted', action="store_false", help="Save only the original stripe image")
    ap.set_defaults(stack_painted=True)

    ap.add_argument("-f", "--frame_path", type=str, default='./backgrounds/bg.jpg', help="Path to static background image")

    ap.add_argument("-u", "--custom_x_origin", type=int, default=397, help="Top left X location for customised frame embedding")
    ap.add_argument("-v", "--custom_y_origin", type=int, default=217, help="Top left Y location for customised frame embedding")

    args = vars(ap.parse_args())

    cp1 = Cropper(im_width=args["origin_width"], im_height=args["origin_length"])

    if not os.path.exists(args["dst_path"]):
        os.mkdir(args["dst_path"], mode = 0o755)

    ls_imgs = glob(args["src_path"])

    frm = cv2.imread(args["frame_path"])

    n_sum_boxes = 0
    for img in ls_imgs:
        im = cv2.imread(img)
        img_name = img.split('/')[-1]
        crop_im_name = args["dst_path"]+img_name

        txt_name = img.replace('.jpg','.txt')

        if args["mode"] == 'crop':
            im1 = cp1.crop_origin(im, x_radius=args["crop_width"], y_radius=args["crop_length"])
        elif args["mode"] == 'crops':
            #print("Stack painted from args: ", args["stack_painted"])
            im1, n_boxes = cp1.crop_by_txt(im, txt_name, save_bbox_crop=args["save_bbox_crop"], stack_painted=args["stack_painted"])
        elif args["mode"] == 'mask':
            im1 = cp1.mask_rects(im, args["rects"])
        elif args["mode"] == 'square':
            im1 = cp1.padding_to_square(im)
        elif args["mode"] == 'frame':
            im1 = cp1.padding_with_frame(im, frm)
        elif args["mode"] == 'frames':
            frm_path = img.replace('images', 'frames')
            #frm_path = cp1.find_frame_path(args["src_path"], args["frame_path"], img)
            frm = cv2.imread(frm_path)
            im1 = cp1.padding_with_frame(im, frm, custom_ind_x_tl=args["custom_x_origin"], custom_ind_y_tl=args["custom_y_origin"])
        
        if im1 is not None:
            cv2.imwrite(crop_im_name, im1)
        else:
            print('No bounding box loaded for: ', txt_name)

        n_sum_boxes += n_boxes

    print(n_sum_boxes)


