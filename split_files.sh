#!/bin/bash
# Courtesy of https://stackoverflow.com/questions/29116212/split-a-folder-into-multiple-subfolders-in-terminal-bash-script
#
# Adjust parameters if necessary {dir_size, dir_name}
# Copy and paste this file to the folder one level above the destination folder
# In terminal, cd to the destination folder
# Run 'sh ../split_files.sh'
# The script will generate dir_name1, dir_name2, dir_name3, ... with each containing dir_size files

dir_size=2000
dir_name="oct2dec2016-batch"
n=$((`find . -maxdepth 1 -type f | wc -l`/$dir_size+1))
for i in `seq 1 $n`;
do
    mkdir -p "$dir_name$i";
    find . -maxdepth 1 -type f | head -n $dir_size | xargs -i mv "{}" "$dir_name$i"
done




### Split Files: An Alternative Implementation (BASH)  Start (*)

# Courtesy of https://askubuntu.com/questions/584724/split-contents-of-a-directory-into-multiple-sub-directories
# 
# Copy this script to the directory containing large number of images using terminal 'cp SplitFile.sh <your_destination_folder>'
# Adjust subSize if necessary, it controls how many files are in each subfolder
# Run this script 'bash SplitFile.sh'
# The script will generate dir_001, dir_002, dir_003 ...
# Change %03d to %04d, %05d, %06d ... in case you need more subfolders
# 
#

# Note that BASH command 'bash' needs to be used here, not SHELL command 'sh'

# Remove the : ' next line and ' one line after done to use this method.
# Remember to comment the above method using the (: ') and (') pair
: '

i=0; 
subSize=10;
for f in *; 
do 
    d=dir_$(printf %03d $((i/subSize+1))); 
    mkdir -p $d; 
    mv "$f" $d; 
    let i++; 
done

'

### Split Files: An Alternative Implementation (BASH)  End (X)
