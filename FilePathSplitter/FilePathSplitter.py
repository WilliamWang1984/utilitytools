import os

class FilePathSplitter:


    def __init__(self, delimiter='/'):
        self.delimiter = delimiter


    # fps = FilePathSplitter(delimiter='_')
    # ['example','string'] = fps.split('example_string')
    def split(self, input_string):
        if type(input_string) is str:
            return input_string.split(self.delimiter)
        else:
            raise Exception('Input must be a string')


    # fps = FilePathSplitter(delimiter='.')
    # 'example' = fps.strip_file_extension('example.jpg') 
    def strip_file_extension(self, input_string):
        pre, dili, post = input_string.rpartition(self.delimiter)
        return pre


    def create_dir_path(self, dir_path, append_sub='saved_txts'):
        ls_paths = dir_path.split(self.delimiter)
        ls_paths.insert(-1, append_sub)

        #print(ls_paths)
        #print(ls_paths[0:-1])

        target_dir = '/'.join(ls_paths[0:-1])+'/'
        if not os.path.isdir(target_dir):
            os.mkdir(target_dir)

        return '/'.join(ls_paths)
