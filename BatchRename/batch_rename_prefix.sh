# Courtesy of:
# https://askubuntu.com/questions/47587/renaming-multiple-files-by-adding-a-string-to-the-beginning
#!bin/sh

# batch rename all files in current directory, appending [pattern] at the beginning of each file

rename 's/^/[prefix-pattern]/' *
