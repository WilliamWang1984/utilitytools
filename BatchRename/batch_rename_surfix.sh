# Courtesy of:
# https://unix.stackexchange.com/questions/56810/adding-text-to-filename-before-extension
#! bin/sh

for f in *.jpg; do printf '%s\n' "${f%.jpg}_[surfix-pattern].jpg"; done

